<?php

namespace EntNewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\RedirectableUrlMatcher;

class EntNewsController extends Controller
{
    /**
     * @Route("/news/index",name="newsindex")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        //获取需要查询的sql和对应的条件
        $arrNews= $em->getRepository('AppBundle:EntNews')->getAllDataOrderByName();
        return $this->render('EntNewsBundle:news:index.html.twig',array('arrnews' => $arrNews));
    }

    /**
     * @Route("/news/add",name="newadd")
     */
    public function addAction(Request $request)
    {
        if($request->isMethod('post')){//是post提交的情况下时新闻数据的新增
            $_arrFormValue = $_POST;
            $em = $this->getDoctrine()->getManager();
            $_objEntNews = $em->getRepository('AppBundle:EntNews')->setAllNameValue($_arrFormValue,'add',null,$this->getParameter('newsstatus_normal'));
            if(is_object($_objEntNews)){
                $em->persist($_objEntNews);
                $em->flush();
                return $this->redirectToRoute('newsindex');
            }else{

            }
        }else{
            $em = $this->getDoctrine()->getManager();
            $_arrEntCai = $em->getRepository('AppBundle:EntCai')->findAll();
            return $this->render('EntNewsBundle:news:add.html.twig',array('arrCai' => $_arrEntCai));
        }
    }
    
}
