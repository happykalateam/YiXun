<?php
namespace CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use CommonBundle\Controller\Aes;

class IAuth extends Controller{

    /**
     * 设置密码
     * @param string $data
     * @return string
     */
    public  function setPassword($data) {
        return md5($data.$this->getParameter('password_pre_halt'));
    }

    /**
     * 生成每次请求的sign
     * @param array $data
     * @return string
     */
    public static function setSign($data = []) {
        // 1 按字段排序
        ksort($data);
        // 2拼接字符串数据  &
        $string = http_build_query($data);
        // 3通过aes来加密
        $string = (new Aes())->encrypt($string);
        return $string;
    }
}