<?php
namespace AppBundle\Service\lib;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Symfony\Component\Config\Definition\Exception\Exception;

class DBOperate{

    /**
     * @abstract 数据库连接对象
     */
    private $link;

    /**
     * @abstract 连接参数
     * @var array
     */
    public $_arrParam = array();

    /**
     * @abstract 保存类实例的私有静态变量
     */
    private static $_instance;
    /**
     * DBOperate constructor.
     * @abstract 私有的构造函数只能够被自身调用，不能够通过new来创建对象
     * @param $__arrParam 数据库连接信息参数，参数基础格式如下
     * array(
                'dbname' => 'abssrtemp',
                'user' => 'postgres',
                'password' => 'abscd123',
                'host' => '192.168.25.128',
                'port' => '5432',
                'driver' => 'pdo_pgsql',
            );
     */
    private function __construct($__arrParam)
    {
        $this->_arrParam = $__arrParam;
        $config = new Configuration();
        $this->link = DriverManager::getConnection($__arrParam, $config);
        if(!$this->link){//连接信息为空
            throw new Exception('连接失败');
        }
    }


    /**
     * @absatract 定义私有的克隆方法，避免单例对象被复制
     */
    private function __clone(){}

    /**
     * @abstract 函数用来获取实例化的唯一对象，避免重复创建对象
     * @param $__arrParam 对象实例化的参数
     * @return DBOperate
     */
    public static function getInstance($__arrParam = array()){
        if(count($__arrParam) == 0){//说明参数传递为空，或者没有传递参数
            $__arrParam = array(
                'dbname' => 'abssrtemp',
                'user' => 'postgres',
                'password' => 'abscd123',
                'host' => '192.168.25.128',
                'port' => '5432',
                'driver' => 'pdo_pgsql',
            );
        }
        //检查类是否已经被实例化了,没有实例化的情况下实例化对象
        if(!(self::$_instance instanceof self)){
            self::$_instance = new DBOperate($__arrParam);
        }
        return self::$_instance;
    }


    /**
     * @param $__strSql  查询sql
     * @param $__arrParam 查询条件参数
     */
    public function getQuery($__strSql,$__arrParam){
        $_arrInfo = $this->link->fetchAll($__strSql,$__arrParam);
        return $_arrInfo;
    }
}