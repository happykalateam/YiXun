<?php
namespace AppBundle\Service\lib;

/**
 * @abstract redis连接和操作类
 * Class RedisOperate
 * @package AppBundle\Service\lib
 */
class RedisOperate{

    /**
     * @abstract redis连接参数
     * @var array
     */
    public $_arrRedisConnectParam = array();

    /**
     * @abstract redis连接对象
     * @var object
     */
    private $_redisLink = '';

    /**
     * @abstract 保存类对象的私有静态变量
     */
    private static $instance;

    /**
     * RedisOperate constructor.
     * @abstract 私有化的构造函数避免外部使用new来创建类对象
     * @param $__srrParam  redis连接配置数组
     */
    private function __construct($__strParam)
    {
        $this->_arrRedisConnectParam = $__strParam;
        $this->_redisLink = new \Redis();
        $this->_redisLink->connect($__strParam['host'],$__strParam['port']);
    }


    /**
     * @abstract 定义私有化的克隆方法，避免对象的复制
     */
    private function __clone(){}


    /**
     * @abstract 实例化唯一的类对象避免重复创建对象
     * @param array $__arrParam
     * @return RedisOperate
     */
    public static function getInstance($__arrParam = array())
    {
        if(count($__arrParam) == 0){//说明调用的时候没有传递参数或者参数传递为空
            $__arrParam = array(
                'host' => '192.168.25.128',
                'port' => '6379'
            );
        }
        //判断当前是否已经创建了对应的对象
        if(!(self::$instance instanceof self)){
            self::$instance = new RedisOperate($__arrParam);
        }
        return self::$instance;
    }

    /**
     * @abstract 函数用来存储header头中的某一个具体的信息，上面的函数saveSignByRedis的功能可以使用当前的这个函数进行替代，只是调用发放要做一定的修改
     * @param $_strStoreInfo 存储值
     * @param $__strStoreKey 存储键值，当前查询出来的键值
     * @param $__type 使用的数据类型，redis现在是有五种数据类型，分别是字符串（string），哈希表（hash），列表（list）,集合（set），有序集合（zset）
     * @return bool
     */
    function saveInfoByRedis($_strStoreInfo,$__strStoreKey,$__type){
        if($__type == 'set'){//使用集合类型存储
            //判断是否存在在集合中不存在的情况下新增一下
            if($this->_redisLink->sIsMember($__strStoreKey,$_strStoreInfo)){//已经存在那么不在存储这条记录同时说明请求不合法
                return false;
            }else{
                $intCount = $this->_redisLink->sAdd($__strStoreKey,$_strStoreInfo);
                if($intCount != 0){//说明成功的新增
                    return true;
                }else{
                    return false;
                }
            }
        }
    }
}