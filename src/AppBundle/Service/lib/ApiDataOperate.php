<?php
namespace AppBundle\Service\lib;

/**
 * @abstract Api接口中数据的处理类
 * Class ApiDataOperate
 * @package AppBundle\Service\lib
 */

class ApiDataOperate{

    /**
     * ApiDataOperate constructor.
     */
    public function __construct(){
    }

    /**
     * @abstract api接口返回的统一封装函数
     * @param $intStatus  业务状态码
     * @param $strMessage 描述信息
     * @param $arrData 数据
     * @param int $intHttpCode http状态码
     */
    public static function apiEnCode($intStatus,$strMessage,$arrData,$intHttpCode = 200){
        //封装返回数据
        $arrReturnData = array(
            'status' => $intStatus,
            'message' => $strMessage,
            'data' => $arrData,
            'httpcode' => $intHttpCode
        );
        return json_encode($arrReturnData);
    }
}