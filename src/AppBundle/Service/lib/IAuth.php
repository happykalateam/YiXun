<?php
namespace AppBundle\Service\lib;

class IAuth{

    /**
     * 设置密码
     * @param string $data
     * @return string
     */
    public  function setPassword($data) {
        return md5($data);
    }

    /**
     * 生成每次请求的sign
     * @param array $data
     * @return string
     */
    public static function setSign($data = []) {
        // 1 按字段排序
        ksort($data);
        // 2拼接字符串数据  &
        $string = http_build_query($data);
        // 3通过aes来加密
        $string = (new Aes())->encrypt($string);
        return $string;
    }

    /**
     * @abstract 生成app客户端用户登录的token字符串
     * @param $__strPhone 登录使用的电话号码，或者其他的字符串信息，比如用户名
     * @return string
     */
    public static function setApploginToken($__strPhone){
        $_str = md5(uniqid(md5(microtime(true)),true));
        $_str = sha1($_str.$__strPhone);
        return $_str;
    }
}