<?php
namespace AppBundle\Service\lib;

class Time{
    /**
     * @abstract 获取13位的时间戳
     * @return int
     */
    public static function get13TimeStamp(){
        list($time1,$time2) = explode(' ',microtime());
        return $time2.ceil($time1*1000);
    }
}