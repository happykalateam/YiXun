<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntUserNews
 *
 * @ORM\Table(name="ent_user_news", uniqueConstraints={@ORM\UniqueConstraint(name="ent_user_news_pk", columns={"id"})}, indexes={@ORM\Index(name="relationship_5_fk", columns={"ent_id"}), @ORM\Index(name="news_id_index", columns={"news_id"}), @ORM\Index(name="relationship_3_fk", columns={"ent_id2"}), @ORM\Index(name="user_id_index", columns={"user_id"})})
 * @ORM\Entity
 */
class EntUserNews
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="news_id", type="integer", nullable=true)
     */
    private $newsId = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="create_time", type="string", length=255, nullable=true)
     */
    private $createTime = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ent_user_news_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \AppBundle\Entity\EntNews
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EntNews")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ent_id2", referencedColumnName="id")
     * })
     */
    private $ent2;

    /**
     * @var \AppBundle\Entity\EntUser
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EntUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ent_id", referencedColumnName="id")
     * })
     */
    private $ent;



    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return EntUserNews
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    
        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set newsId
     *
     * @param integer $newsId
     *
     * @return EntUserNews
     */
    public function setNewsId($newsId)
    {
        $this->newsId = $newsId;
    
        return $this;
    }

    /**
     * Get newsId
     *
     * @return integer
     */
    public function getNewsId()
    {
        return $this->newsId;
    }

    /**
     * Set createTime
     *
     * @param string $createTime
     *
     * @return EntUserNews
     */
    public function setCreateTime($createTime)
    {
        $this->createTime = $createTime;
    
        return $this;
    }

    /**
     * Get createTime
     *
     * @return string
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ent2
     *
     * @param \AppBundle\Entity\EntNews $ent2
     *
     * @return EntUserNews
     */
    public function setEnt2(\AppBundle\Entity\EntNews $ent2 = null)
    {
        $this->ent2 = $ent2;
    
        return $this;
    }

    /**
     * Get ent2
     *
     * @return \AppBundle\Entity\EntNews
     */
    public function getEnt2()
    {
        return $this->ent2;
    }

    /**
     * Set ent
     *
     * @param \AppBundle\Entity\EntUser $ent
     *
     * @return EntUserNews
     */
    public function setEnt(\AppBundle\Entity\EntUser $ent = null)
    {
        $this->ent = $ent;
    
        return $this;
    }

    /**
     * Get ent
     *
     * @return \AppBundle\Entity\EntUser
     */
    public function getEnt()
    {
        return $this->ent;
    }
}
