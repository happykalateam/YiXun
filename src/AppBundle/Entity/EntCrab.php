<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntCrab
 *
 * @ORM\Table(name="ent_crab", uniqueConstraints={@ORM\UniqueConstraint(name="ent_crab_pk", columns={"id"})}, indexes={@ORM\Index(name="relationship_6_fk", columns={"ent_id"})})
 * @ORM\Entity
 */
class EntCrab
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ent_crab_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \AppBundle\Entity\EntVersion
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EntVersion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ent_id", referencedColumnName="id")
     * })
     */
    private $ent;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ent
     *
     * @param \AppBundle\Entity\EntVersion $ent
     *
     * @return EntCrab
     */
    public function setEnt(\AppBundle\Entity\EntVersion $ent = null)
    {
        $this->ent = $ent;
    
        return $this;
    }

    /**
     * Get ent
     *
     * @return \AppBundle\Entity\EntVersion
     */
    public function getEnt()
    {
        return $this->ent;
    }
}
