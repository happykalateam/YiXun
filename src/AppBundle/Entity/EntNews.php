<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntNews
 *
 * @ORM\Table(name="ent_news", uniqueConstraints={@ORM\UniqueConstraint(name="ent_news_pk", columns={"id"})}, indexes={@ORM\Index(name="relationship_1_fk", columns={"ent_id"})})
 * @ORM\Entity
 */
class EntNews
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=254, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="small_title", type="string", length=254, nullable=true)
     */
    private $smallTitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="caiid", type="integer", nullable=true)
     */
    private $caiid;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=254, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=254, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_position", type="integer", nullable=true)
     */
    private $isPosition;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_head_figure", type="integer", nullable=true)
     */
    private $isHeadFigure;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_allowcomments", type="integer", nullable=true)
     */
    private $isAllowcomments;

    /**
     * @var integer
     *
     * @ORM\Column(name="listorder", type="integer", nullable=true)
     */
    private $listorder;

    /**
     * @var integer
     *
     * @ORM\Column(name="source_type", type="integer", nullable=true)
     */
    private $sourceType;

    /**
     * @var string
     *
     * @ORM\Column(name="create_time", type="string", length=254, nullable=true)
     */
    private $createTime;

    /**
     * @var string
     *
     * @ORM\Column(name="update_time", type="string", length=254, nullable=true)
     */
    private $updateTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="read_count", type="integer", nullable=true)
     */
    private $readCount = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="upvote_count", type="integer", nullable=true)
     */
    private $upvoteCount = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="common_count", type="integer", nullable=true)
     */
    private $commonCount = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ent_news_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \AppBundle\Entity\EntAdminUser
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EntAdminUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ent_id", referencedColumnName="id")
     * })
     */
    private $ent;



    /**
     * Set title
     *
     * @param string $title
     *
     * @return EntNews
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set smallTitle
     *
     * @param string $smallTitle
     *
     * @return EntNews
     */
    public function setSmallTitle($smallTitle)
    {
        $this->smallTitle = $smallTitle;
    
        return $this;
    }

    /**
     * Get smallTitle
     *
     * @return string
     */
    public function getSmallTitle()
    {
        return $this->smallTitle;
    }

    /**
     * Set caiid
     *
     * @param integer $caiid
     *
     * @return EntNews
     */
    public function setCaiid($caiid)
    {
        $this->caiid = $caiid;
    
        return $this;
    }

    /**
     * Get caiid
     *
     * @return integer
     */
    public function getCaiid()
    {
        return $this->caiid;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return EntNews
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return EntNews
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return EntNews
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isPosition
     *
     * @param integer $isPosition
     *
     * @return EntNews
     */
    public function setIsPosition($isPosition)
    {
        $this->isPosition = $isPosition;
    
        return $this;
    }

    /**
     * Get isPosition
     *
     * @return integer
     */
    public function getIsPosition()
    {
        return $this->isPosition;
    }

    /**
     * Set isHeadFigure
     *
     * @param integer $isHeadFigure
     *
     * @return EntNews
     */
    public function setIsHeadFigure($isHeadFigure)
    {
        $this->isHeadFigure = $isHeadFigure;
    
        return $this;
    }

    /**
     * Get isHeadFigure
     *
     * @return integer
     */
    public function getIsHeadFigure()
    {
        return $this->isHeadFigure;
    }

    /**
     * Set isAllowcomments
     *
     * @param integer $isAllowcomments
     *
     * @return EntNews
     */
    public function setIsAllowcomments($isAllowcomments)
    {
        $this->isAllowcomments = $isAllowcomments;
    
        return $this;
    }

    /**
     * Get isAllowcomments
     *
     * @return integer
     */
    public function getIsAllowcomments()
    {
        return $this->isAllowcomments;
    }

    /**
     * Set listorder
     *
     * @param integer $listorder
     *
     * @return EntNews
     */
    public function setListorder($listorder)
    {
        $this->listorder = $listorder;
    
        return $this;
    }

    /**
     * Get listorder
     *
     * @return integer
     */
    public function getListorder()
    {
        return $this->listorder;
    }

    /**
     * Set sourceType
     *
     * @param integer $sourceType
     *
     * @return EntNews
     */
    public function setSourceType($sourceType)
    {
        $this->sourceType = $sourceType;
    
        return $this;
    }

    /**
     * Get sourceType
     *
     * @return integer
     */
    public function getSourceType()
    {
        return $this->sourceType;
    }

    /**
     * Set createTime
     *
     * @param string $createTime
     *
     * @return EntNews
     */
    public function setCreateTime($createTime)
    {
        $this->createTime = $createTime;
    
        return $this;
    }

    /**
     * Get createTime
     *
     * @return string
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * Set updateTime
     *
     * @param string $updateTime
     *
     * @return EntNews
     */
    public function setUpdateTime($updateTime)
    {
        $this->updateTime = $updateTime;
    
        return $this;
    }

    /**
     * Get updateTime
     *
     * @return string
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return EntNews
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set readCount
     *
     * @param integer $readCount
     *
     * @return EntNews
     */
    public function setReadCount($readCount)
    {
        $this->readCount = $readCount;
    
        return $this;
    }

    /**
     * Get readCount
     *
     * @return integer
     */
    public function getReadCount()
    {
        return $this->readCount;
    }

    /**
     * Set upvoteCount
     *
     * @param integer $upvoteCount
     *
     * @return EntNews
     */
    public function setUpvoteCount($upvoteCount)
    {
        $this->upvoteCount = $upvoteCount;
    
        return $this;
    }

    /**
     * Get upvoteCount
     *
     * @return integer
     */
    public function getUpvoteCount()
    {
        return $this->upvoteCount;
    }

    /**
     * Set commonCount
     *
     * @param integer $commonCount
     *
     * @return EntNews
     */
    public function setCommonCount($commonCount)
    {
        $this->commonCount = $commonCount;
    
        return $this;
    }

    /**
     * Get commonCount
     *
     * @return integer
     */
    public function getCommonCount()
    {
        return $this->commonCount;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ent
     *
     * @param \AppBundle\Entity\EntAdminUser $ent
     *
     * @return EntNews
     */
    public function setEnt(\AppBundle\Entity\EntAdminUser $ent = null)
    {
        $this->ent = $ent;
    
        return $this;
    }

    /**
     * Get ent
     *
     * @return \AppBundle\Entity\EntAdminUser
     */
    public function getEnt()
    {
        return $this->ent;
    }
}
