<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntUser
 *
 * @ORM\Table(name="ent_user", uniqueConstraints={@ORM\UniqueConstraint(name="ent_user_pk", columns={"id"})}, indexes={@ORM\Index(name="ent_user_phone", columns={"telephone"}), @ORM\Index(name="ent_user_token", columns={"token"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EntUserRepository")
 */
class EntUser
{
    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=254, nullable=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=254, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="qq", type="string", length=254, nullable=true)
     */
    private $qq;

    /**
     * @var string
     *
     * @ORM\Column(name="weixin", type="string", length=254, nullable=true)
     */
    private $weixin;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=254, nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=254, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="last_login_ip", type="string", length=254, nullable=true)
     */
    private $lastLoginIp;

    /**
     * @var integer
     *
     * @ORM\Column(name="listorder", type="integer", nullable=true)
     */
    private $listorder;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="create_time", type="string", length=254, nullable=true)
     */
    private $createTime;

    /**
     * @var string
     *
     * @ORM\Column(name="update_time", type="string", length=254, nullable=true)
     */
    private $updateTime;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @var integer
     *
     * @ORM\Column(name="timeout", type="integer", nullable=true)
     */
    private $timeout;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var integer
     *
     * @ORM\Column(name="sex", type="integer", nullable=true)
     */
    private $sex;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_login_time", type="integer", nullable=true)
     */
    private $lastLoginTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ent_user_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;



    /**
     * Set username
     *
     * @param string $username
     *
     * @return EntUser
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return EntUser
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set qq
     *
     * @param string $qq
     *
     * @return EntUser
     */
    public function setQq($qq)
    {
        $this->qq = $qq;
    
        return $this;
    }

    /**
     * Get qq
     *
     * @return string
     */
    public function getQq()
    {
        return $this->qq;
    }

    /**
     * Set weixin
     *
     * @param string $weixin
     *
     * @return EntUser
     */
    public function setWeixin($weixin)
    {
        $this->weixin = $weixin;
    
        return $this;
    }

    /**
     * Get weixin
     *
     * @return string
     */
    public function getWeixin()
    {
        return $this->weixin;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return EntUser
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return EntUser
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set lastLoginIp
     *
     * @param string $lastLoginIp
     *
     * @return EntUser
     */
    public function setLastLoginIp($lastLoginIp)
    {
        $this->lastLoginIp = $lastLoginIp;
    
        return $this;
    }

    /**
     * Get lastLoginIp
     *
     * @return string
     */
    public function getLastLoginIp()
    {
        return $this->lastLoginIp;
    }

    /**
     * Set listorder
     *
     * @param integer $listorder
     *
     * @return EntUser
     */
    public function setListorder($listorder)
    {
        $this->listorder = $listorder;
    
        return $this;
    }

    /**
     * Get listorder
     *
     * @return integer
     */
    public function getListorder()
    {
        return $this->listorder;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return EntUser
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createTime
     *
     * @param string $createTime
     *
     * @return EntUser
     */
    public function setCreateTime($createTime)
    {
        $this->createTime = $createTime;
    
        return $this;
    }

    /**
     * Get createTime
     *
     * @return string
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * Set updateTime
     *
     * @param string $updateTime
     *
     * @return EntUser
     */
    public function setUpdateTime($updateTime)
    {
        $this->updateTime = $updateTime;
    
        return $this;
    }

    /**
     * Get updateTime
     *
     * @return string
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return EntUser
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set timeout
     *
     * @param integer $timeout
     *
     * @return EntUser
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
    
        return $this;
    }

    /**
     * Get timeout
     *
     * @return integer
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return EntUser
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set sex
     *
     * @param integer $sex
     *
     * @return EntUser
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    
        return $this;
    }

    /**
     * Get sex
     *
     * @return integer
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set lastLoginTime
     *
     * @param integer $lastLoginTime
     *
     * @return EntUser
     */
    public function setLastLoginTime($lastLoginTime)
    {
        $this->lastLoginTime = $lastLoginTime;
    
        return $this;
    }

    /**
     * Get lastLoginTime
     *
     * @return integer
     */
    public function getLastLoginTime()
    {
        return $this->lastLoginTime;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
