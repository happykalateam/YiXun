<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntCai
 *
 * @ORM\Table(name="ent_cai")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EntCaiRepository")
 */
class EntCai
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="caiitem", type="string", length=255)
     */
    private $caiitem;

    /**
     * @var string
     *
     * @ORM\Column(name="caicode", type="string", length=255)
     */
    private $caicode;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set caiitem
     *
     * @param string $caiitem
     *
     * @return EntCai
     */
    public function setCaiitem($caiitem)
    {
        $this->caiitem = $caiitem;
    
        return $this;
    }

    /**
     * Get caiitem
     *
     * @return string
     */
    public function getCaiitem()
    {
        return $this->caiitem;
    }

    /**
     * Set caicode
     *
     * @param string $caicode
     *
     * @return EntCai
     */
    public function setCaicode($caicode)
    {
        $this->caicode = $caicode;
    
        return $this;
    }

    /**
     * Get caicode
     *
     * @return string
     */
    public function getCaicode()
    {
        return $this->caicode;
    }
}
