<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntAppActive
 *
 * @ORM\Table(name="ent_app_active", uniqueConstraints={@ORM\UniqueConstraint(name="ent_app_active_pk", columns={"id"})}, indexes={@ORM\Index(name="relationship_7_fk", columns={"ent_id"})})
 * @ORM\Entity
 */
class EntAppActive
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ent_app_active_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \AppBundle\Entity\EntVersion
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EntVersion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ent_id", referencedColumnName="id")
     * })
     */
    private $ent;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ent
     *
     * @param \AppBundle\Entity\EntVersion $ent
     *
     * @return EntAppActive
     */
    public function setEnt(\AppBundle\Entity\EntVersion $ent = null)
    {
        $this->ent = $ent;
    
        return $this;
    }

    /**
     * Get ent
     *
     * @return \AppBundle\Entity\EntVersion
     */
    public function getEnt()
    {
        return $this->ent;
    }
}
