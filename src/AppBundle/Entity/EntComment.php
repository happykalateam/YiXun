<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntComment
 *
 * @ORM\Table(name="ent_comment", uniqueConstraints={@ORM\UniqueConstraint(name="ent_comment_pk", columns={"id"})}, indexes={@ORM\Index(name="relationship_2_fk", columns={"ent_id"}), @ORM\Index(name="relationship_4_fk", columns={"ent_id2"})})
 * @ORM\Entity
 */
class EntComment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ent_comment_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \AppBundle\Entity\EntNews
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EntNews")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ent_id", referencedColumnName="id")
     * })
     */
    private $ent;

    /**
     * @var \AppBundle\Entity\EntUser
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EntUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ent_id2", referencedColumnName="id")
     * })
     */
    private $ent2;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ent
     *
     * @param \AppBundle\Entity\EntNews $ent
     *
     * @return EntComment
     */
    public function setEnt(\AppBundle\Entity\EntNews $ent = null)
    {
        $this->ent = $ent;
    
        return $this;
    }

    /**
     * Get ent
     *
     * @return \AppBundle\Entity\EntNews
     */
    public function getEnt()
    {
        return $this->ent;
    }

    /**
     * Set ent2
     *
     * @param \AppBundle\Entity\EntUser $ent2
     *
     * @return EntComment
     */
    public function setEnt2(\AppBundle\Entity\EntUser $ent2 = null)
    {
        $this->ent2 = $ent2;
    
        return $this;
    }

    /**
     * Get ent2
     *
     * @return \AppBundle\Entity\EntUser
     */
    public function getEnt2()
    {
        return $this->ent2;
    }
}
