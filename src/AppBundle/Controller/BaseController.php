<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{
    /**
     * @var page
     * 当前页数
     */
    public $page = '';

    /**
     * @var size
     * 每页显示的条数
     */
    public $size = '';

    /**
     * @var int
     * 从那一页开始做查询
     */
    public $from = 0;

    public function __construct(){
//        print_r($_SERVER);exit;
//        print_r($this->getParameter('headers_validate'));exit;
//        $this->checkHeaderInfo($_SERVER);
    }

    /**
     * @author LF
     * @abstract 判断当前的用户是否已经做了登录
     * @param Request $request
     */
    public  function isLogin(Request $request){
        $_objSession = $request->getSession();
        $_objUser = $_objSession->get($this->getParameter('user_session_scope'));
        if($_objUser && $_objUser->getId()){
            //当前是用户登录状态，不用做任何操作
        }else{
            //当前用户没有登录的情况下，跳转到的登录页面
            return $this->redirect('login');
        }
    }

    /**
     * @author lf
     * @abstract 获取当前的页和每页显示的条数
     */
    public function getPageAndSize($data){
        $this->page = !empty($data['page'])?$data['page']:1;//当前页
        $this->size = !empty($data['size'])?$data['size']:$this->getParameter('list_rows');//每页显示的条数
        $this->from = ($this->page - 1)*$this->size;//从那一页开始
    }

    /**
     * @author lf
     * @abstract 处理&转为&amp;的情况
     * @param $arrData
     */
    public function build_query($arrData){
        foreach($arrData as $key=>$value){
            if(strpos($key,'amp;') !== false){//存在
                $_strNewKey = str_replace('amp;','',$key);
                $arrData[$_strNewKey] = $value;
                unset($arrData[$key]);
            }
        }
        return $arrData;
    }

    /**
     **@abstract 验证http请求的header头信息，
     * 请求头信息包含以下的内容sign 请求参数加密字符串  version app版本参数  did 请求设备编码  app_type 应用平台参数
     * os 操作系统标识   model  设备型号  access_user_token 用户数据请求token
     * @param $_arrServer
     * @return mixed
     */
    public function checkHeaderInfo(Request $request){
        //设置是否验证的请求头的参数
        $_boolHeader = false;
        if($_boolHeader){
            //获取请求头
            $_objHeader = $request->headers;
            //验证几个基础参数比如sign，did
            $t = $_objHeader->get('sign');
            if(empty($_objHeader->get('sign'))){//sign验签不存在
                return $this->get('common_service')->apiEnCode('0','sign不合法',[],400);
            }
            //验证设备号
            if(empty($_objHeader->get('did'))){//设备号不存在
                return $this->get('common_service')->apiEnCode('0','did数据不合法',[],400);
            }
            //校验sign和传递参数的值是否合法
            $_boolPass = $this->get('common_service')->checkSignPass($_objHeader);
            if($_boolPass){
                return $this->get('common_service')->apiEnCode('1','sign验证合法',[],200);
            }else{
                return $this->get('common_service')->apiEnCode('0','sign验证不合法',[],400);
            }
        }else{
            return $this->get('common_service')->apiEnCode('1','sign验证合法',[],200);
        }
    }
}
