<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\EntNews;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        //获取当前登录的用户
        $_objSession = $request->getSession();
        $_objLoginUser = $_objSession->get($this->getParameter('user_session_scope'));
        if(is_object($_objLoginUser)){
            return $this->render('default\base.html.twig',array('loginuser' => $_objLoginUser));
        }else{
            return $this->redirectToRoute('login');
        }

    }
}
