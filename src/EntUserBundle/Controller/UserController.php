<?php

namespace EntUserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Controller\BaseController;


class UserController extends BaseController
{
    //使用参数判断当前是进入用户的新增页面还是编辑页面
    /**
     * @Route("/adminuser/edit/{flag}/{id}",name="adminuser_add")
     * @param $flag
     * @param int $id
     */
    public function addoreditAction($flag,$id=0)
    {
        if($flag == 'add'){//管理员用户的新增
            return $this->render('EntUserBundle:User:add.html.twig');
        }else if($flag == 'update'){//已经存在的管理员用户的编辑
            $_arrUserData = array();
            return $this->render('EntUserBundle:User:add.html.twig',array('userarr' => $_arrUserData));
       }
    }
    //用户数据存储控制器
    /**
     * @Route("/adminuser/save",name="adminuser_save")
     */
    public function usersaveAction()
    {
        $_arrFormValue = $_POST;
        $em = $this->getDoctrine()->getManager();
        $_objEntAdminUser = $em->getRepository('AppBundle:EntAdminUser')->setAllNameValue($_arrFormValue,'add',null,$this->getParameter('userstatus_normal'));
        if(is_object($_objEntAdminUser)){
            $em->persist($_objEntAdminUser);
            $em->flush();
            return $this->redirectToRoute('adminuser_list');
        }/*else{

        }*/
    }
    //用户展示页面
    /**
     * @Route("/adminuser/list",name="adminuser_list")
     */
    public function listAction(Request $request)
    {
        $arrCondition = array();
        //获取路由传递的参数
        $arrCondition = $request->query->all();
        foreach($arrCondition as $key=>$value){
            if(is_null($value) || $value == '' ||  $value == null){
                unset($arrCondition[$key]);
            }
        }
        //region 处理将html返回的&amp;替换为&，这里有两种方式做示例
        //方式一在当前控制器或者父级控制器中定义函数处理(不建议使用方式一)
        //$arrCondition = $this->build_query($arrCondition);
        //方式二使用服务容器处理
        $arrCondition = $this->get('common_service')->build_query($arrCondition);
        //endregion


        //设置查询的起始页和每次查询的条数
        $this->getPageAndSize($arrCondition);
        //计算出分页参数之后将page去掉以免影响数据的查询
        unset($arrCondition['page']);
        //拼接搜索查询字符串
        $_strQurey = urldecode(http_build_query($arrCondition));
        $em = $this->getDoctrine()->getManager();
        //获取需要查询的sql和对应的条件
        $arrSqlAndCondition = $em->getRepository('AppBundle:EntAdminUser')->getSql($arrCondition);
        $_strSql = $arrSqlAndCondition['sql'];
        unset($arrSqlAndCondition['sql']);
        $arrAdminUser = $em->getRepository('AppBundle:EntAdminUser')->getDataByConition($_strSql,$arrSqlAndCondition,$this->from,$this->size);
        //查询满足条件的所有的数据数量
        $intCountAdminUser = $em->getRepository('AppBundle:EntAdminUser')->getDataByConditionCount($_strSql,$arrSqlAndCondition);
        //计算出能够分页的数量
        $intTotalPage = ceil($intCountAdminUser/$this->size);
        //将查询数据中最后一次登录的时间转换为字符串显示
        foreach($arrAdminUser as $key=>$value){
            $_objDate = $value->getLastLoginDate();
            if(is_object($_objDate)){
                $value->setLastLoginDate($_objDate->format('Y-m-d'));
            }
            $arrAdminUser[$key] = $value;
        }
//        $arrAdminUserOperateUrl = array();
//        foreach($arrAdminUser as $key=>$value){
//            $arrAdminUserOperateUrl[$value->getid()]['del'] = $this->generateUrl('adminuser_show',array('id'=>$value->getid()));
//        }
        //循环 查询的对象将值取出存储到数组中
        return $this->render('EntUserBundle:User:list.html.twig',array(
                              'ShowAdminUserData' => $arrAdminUser,
                              'pageTotal' => $intTotalPage,
                              'Total'=>$intCountAdminUser,
                              'curr' => $this->page,
                              'query' => $_strQurey,
                              'start_time' => empty($arrCondition['start_time']) ? '' : $arrCondition['start_time'],
                              'end_time' => empty($arrCondition['end_time']) ? '' : $arrCondition['end_time'],
                              'username' => empty($arrCondition['username']) ? '' : $arrCondition['username'],
                             ));
//        return $this->render('EntUserBundle:User:list.html.twig',array('ShowAdminUserData' => $arrAdminUser,'arrAdminUserOperateUrl' => $arrAdminUserOperateUrl));
    }

    /**
     * @Route("/adminuser/show",name="adminuser_show")
     * @param Request $request
     * @return Response
     */
    public function showAction(Request $request)
    {
        $arrCondition = array();
        //获取路由传递的参数
        $arrCondition = $request->query->all();
        foreach($arrCondition as $key=>$value){
            if(is_null($value) || $value == '' ||  $value == null){
                unset($arrCondition[$key]);
            }
        }
       
        //分页数据查询
        $objEntityManager = $this->get('doctrine.orm.entity_manager');//获取实体管理器

        //获取需要查询的sql和对应的条件
        $arrSqlAndCondition = $objEntityManager->getRepository('AppBundle:EntAdminUser')->getSql($arrCondition);
        $_strSql = $arrSqlAndCondition['sql'];
        unset($arrSqlAndCondition['sql']);
        
        $query = $objEntityManager->createQuery($_strSql)->setParameters($arrSqlAndCondition);
        $paginator  = $this->get('knp_paginator');
        //第一个参数为查询的sql创建的Query 第二个参数表示从第几页开始查询，第三个参数标识每页的查询数量
        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), 3);
        //获取其中的item数据将其中的data类型处理成字符串形式
        $_arrItems = $pagination->getItems();
        foreach($_arrItems as $key=>$value){
            $_objDate = $value->getLastLoginDate();
            if(is_object($_objDate)){
                $value->setLastLoginDate($_objDate->format('Y-m-d'));
            }
            $_arrItems[$key] = $value;
        }
        $pagination->setItems($_arrItems);
        return $this->render('EntUserBundle:User:show.html.twig', array('pagination' => $pagination));
    }
}
