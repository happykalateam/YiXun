<?php
/**
 * Created by PhpStorm.
 * User: lf
 * Date: 2018/1/15
 * Time: 18:07
 */
namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiTimeController
 * @package ApiBundle\Controller
 */
class ApiTimeController extends Controller{

    /**
     * @abstract 获取服务器的时间戳
     * @Route("/server/time",name="server_time")
     */
    public function getServerTime(){
        $_intTime = time();
        $_strReturn = $this->get('common_service')->apiEnCode('1','服务器时间请求成功',$_intTime,200);
        return new Response($_strReturn);
        
    }


}