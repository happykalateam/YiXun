<?php
namespace ApiBundle\Controller;

use AppBundle\Entity\EntUser;
use AppBundle\Service\lib\Aes;
use AppBundle\Service\lib\ApiDataOperate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ApiBundle\Controller\AuthBaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


/**
 * @abstract 用户管理相关的数据接口操作控制器类
 * @package ApiBundle\Controller
 */
class ApiUserController extends AuthBaseController{

    /**
     * @abstract 获取所有的用户数据
     * @Route("/adminuser/all",name="adminuser_all")
     */
    function getAllUserData(Request $request){
        //验证数据请求头
        $_CheckInfo = $this->checkHeaderInfo($request);
        //使用json转码
        $_objCheckInfo = json_decode($_CheckInfo);
        if($_objCheckInfo->status == '0'){//数据验证失败
            $_strReturn = $this->get('common_service')->apiEnCode($_objCheckInfo->status,$_objCheckInfo->message,[],400);
            return new Response($_strReturn);
        }else if($_objCheckInfo->status == '1'){//请求数据验证成功
            //在数据验签成功的情况下使用redis记录一下请求中的验签的sign值，作为下一次验签的验证准备
            $_boolSaveSignByRedis = $this->get('common_service')->saveSignByRedis($request);
            if($_boolSaveSignByRedis){//标识存储成功，下一步及时查询实际的数据了
                $_strReturn = $this->get('common_service')->apiEnCode($_objCheckInfo->status,$_objCheckInfo->message,[],200);
            }else{
                $_strReturn = $this->get('common_service')->apiEnCode('0','sign已失效或存储失败',[],400);
            }
            return new Response($_strReturn);
        }
    }

    /**
     * @abstract 更新用户的密码，客户端调用这个接口更新对应用户的密码
     * @Route("/user/update/password",name="adminuser_update_password")
     */
    function updateUserPassword(Request $request){
        $_strReturn = '';
        //验证header头所带的信息是否合法
        $_strCheckInfo = $this->checkHeaderInfo($request);
        $_objCheckInfo = json_decode($_strCheckInfo);
        if($_objCheckInfo->status == 0){//header头数据验证失败
            $_strReturn = $this->get('common_service')->apiEnCode($_objCheckInfo->status,$_objCheckInfo->message,[],400);
        }else if($_objCheckInfo->status == 1) {//header头数据验证成功
            $_arrUpdateParams = $request->request->all();
            if(array_key_exists('phone',$_arrUpdateParams) && $_arrUpdateParams['phone'] != '' && !is_null($_arrUpdateParams['phone'])){
                //验证请求的时间和验证码的过期时间之间的大小,请求时间大于过期时间的时候给出提示
                if($_arrUpdateParams['time'] > $_SESSION[$_arrUpdateParams['phone']."validatecode_timeout"]){
                    $_strReturn = $this->get('common_service')->apiEnCode('0','验证码已过期','',400);
                }
                //验证当前输入的验证码和session中存储的验证码是否是一致的
                if($_arrUpdateParams['code'] != $_SESSION[$_arrUpdateParams['phone']."validatecode"]){
                    $_strReturn = $this->get('common_service')->apiEnCode('0','验证码不正确','',400);
                }
                //验证数据库中是否存在电话未传递参数标记值的用户
                $em = $this->getDoctrine()->getManager();
                //根据手机号码进行查询
                $_objUser = $em->getRepository('AppBundle:EntUser')->findOneByTelephone($_arrUpdateParams['phone']);
                if($_objUser){
                    $_objUser->setPassword($_arrUpdateParams['password']);
                    $em->flush();
                    $_strReturn = $this->get('common_service')->apiEnCode('1','更新成功',$_objUser->getId(),200);
                }else{
                    $_strReturn = $this->get('common_service')->apiEnCode('0','该用户不存在更新失败','',400);
                }
            }else{
                $_strReturn = $this->get('common_service')->apiEnCode('0','密码更新失败，参数错误','',400);
            }
        }
        return new Response($_strReturn);
    }


    /**
     * @abstract 更新用户的其他信息,这里提供用户名，性别，邮箱三个字段的更新，后期可以扩展这个函数
     * 在调用这个接口的时候，前端页面的空间中name属性请严格按照实体类中的字段来处理
     * (1)复用这个路由实现密码的添加，用户使用免密登录的情况下直接跳转到了用户中心，这个时候用户在增加自己的密码，直接调用这个接口就可以了
     * @Route("/update/appuser/other/info",name="update_user_info")
     */
    function updateUserOtherInfo(Request $request){
        //获取请求的数据
        $_arrRequestData = $request->request->all();
        //剔除字段为空的数据
        $_objAes = new Aes();
        foreach($_arrRequestData as $key=>$value){
            if(is_null($value) || $value == '' || $value == null){
                unset($_arrRequestData[$key]);
            }else{//对于部位空的数据调用后端解密方法对数据进行解密处理
                $_arrRequestData[$key] = $_objAes->cryptoJsAesDecrypt($_objAes->key,$value);
            }
        }
        //判断处理之后的数组是否为空，为空的情况下给出提示信息
        $_arrRequestData = array();
        if(count($_arrRequestData) == 0){
            $_strReturn = ApiDataOperate::apiEnCode('0','提交数据错误',[],400);
        }else{
            //正常通过的情况下获取当前用户的对象
            $em = $this->getDoctrine()->getManager();
            $_objUser = $em->getRepository('AppBundle:EntUser')->find($this->_arrUser['id']);
            //对象的循环赋值过程
            foreach($_arrRequestData as $key=>$value){
                $_strMethod = 'set'.$key;
                if(method_exists($_objUser,$_strMethod)){
                    $_objUser->$_strMethod($value);
                }
            }
            //更新时间存储
            $_objUser->setUpdateTime(time());
            $em->flush();
            $_strReturn = ApiDataOperate::apiEnCode('1','更新成功',[],200);
        }
        return new Response($_strReturn);
    }

    /**
     * @abstract 验证用户名是否已经存在的接口
     * @Route("/validate/app/user",name="validate_name")
     */
     public function validateAppUserName(Request $request){
         $em = $this->getDoctrine()->getManager();
         $_objUser = $em->getRepository('AppBundle:EntUser')->findBy(array('username' => $request->get('username')));
         if($_objUser){//用户名传递的已经存在
            $_strReturn = ApiDataOperate::apiEnCode(0,'该用户名已经存在',[],200);
         }else{//不存在可以通过
            $_strReturn = ApiDataOperate::apiEnCode(1,'该用户名可用',[],200);
         }
         return new Response($_strReturn);
     }
    /**
     * @abstract 获取当前登录用户的具体信息
     * @Route("/current/login/user/info",name="current_user_info")
     */
    function currentUserInfo(){
        return new Response(ApiDataOperate::apiEnCode('1','获取成功',$this->_arrUser,200));
    }
}