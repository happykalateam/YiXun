<?php
namespace ApiBundle\Controller;

use AppBundle\Entity\EntUser;
use AppBundle\Entity\EntUserNews;
use AppBundle\Service\lib\Aes;
use AppBundle\Service\lib\ApiDataOperate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ApiBundle\Controller\AuthBaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class ApiUpvoteController
 * @package ApiBundle\Controller
 */
class ApiUpvoteController extends  AuthBaseController{

    /**
     * @abstract 用户点赞接口
     * @Route("/user/news/upvote",name="news_upvote")
     */
    public function newUpvote(Request $request){
        //获取post请求过来的数据
        $_arrPostData = $request->request->all();
        if(is_null($_arrPostData['new_id']) || $_arrPostData['new_id'] == ''){//传递的新闻id为空
            $_strReturn = ApiDataOperate::apiEnCode('0','新闻id为空，点赞失败',[],400);
            return new Response($_strReturn);
        }
        //查询传递的新闻是否存在且状态正常
        $em = $this->getDoctrine()->getManager();
        $_objNews = $em->getRepository('AppBundle:EntNews')->find($_arrPostData['new_id']);
        //新闻数据存在且状态为正常
        if($_objNews && $_objNews->getStatus() == $this->getParameter('newsstatus_normal')){
            //判断当前用户是否已经对这个新闻做了点赞
            $_arrData = array('userId' => $this->_arrUser['id'],'newsId' => $_arrPostData['new_id']);
            $_objData = $em->getRepository('AppBundle:EntUserNews')->findBy($_arrData);
            if($_objData){//已经点赞过了
                $_strReturn = ApiDataOperate::apiEnCode(0,'已经点赞过了',[],200);
                return new Response($_strReturn);
            }else{//还没有点赞过
                $_objEntUserNews = new EntUserNews();
                $_objEntUserNews->setNewsId($_arrPostData['new_id']);
                $_objEntUserNews->setUserId($this->_arrUser['id']);
                $em->persist($_objEntUserNews);
                $em->flush();
                if($_objEntUserNews->getId()){//说明数据存储成功
                    //利用数据存储更新新闻信息中的点赞数量字段，这个理想化的而处理是使用redis来操作,redis后面查询方案了再来解决
                    try{
                        $_objNews = $em->getRepository('AppBundle:EntNews')->find($_arrPostData['new_id']);
                        $_objNews->setUpvoteCount(intval($_objNews->getUpvoteCount()) + 1);
                        $em->flush();
                        $_strReturn = ApiDataOperate::apiEnCode(1,'点赞成功',[],200);
                        return new Response($_strReturn);
                    }catch(\Exception $e){
                        $_strReturn = ApiDataOperate::apiEnCode(0,'内部错误点赞失败',[],400);
                        return new Response($_strReturn);
                    }
                }else{//点赞失败
                    $_strReturn = ApiDataOperate::apiEnCode(0,'点赞失败，请重试',[],400);
                    return new Response($_strReturn);
                }
            }
        }else{
            $_strReturn = ApiDataOperate::apiEnCode('0','新闻数据失效，点赞失败',[],400);
            return new Response($_strReturn);
        }
    }

    /**
     * @abstract 取消点赞
     * @Route("/user/news/unupvote",name="news_unupvote")
     */
    public function unNewsUpvote(){

    }

    /**
     * @abstract 查询当前新闻数据是否被当前用户点赞了
     * @Route("user/news/voteornot",name="news_voteornot")
     */
    public function getNewVoteInfo(){

    }
}