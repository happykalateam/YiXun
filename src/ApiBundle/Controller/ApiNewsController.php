<?php
/**
 * Created by PhpStorm.
 * User: AbsCD
 * Date: 2018/2/7
 * Time: 10:48
 */
namespace ApiBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiNewsController extends ApiCommonController
{
    /**
     * @abstract 新闻栏目接口
     * @author chenyl
     * @Route("/news_bundle/news_type",name="news_type")
     */
    public function newsTypeAction(Request $req){
        //将通用服务保存到对象
        $common_service = $this->get('common_service');

        //验证请求的合法性，获取返回的Json字符串
        $return_json = $this->checkHeaderInfo($req);

        //将校验结果解码
        $return_obj = json_decode($return_json);
        //检查请求是否合法,不合法则返回非法信息
        if($return_obj->status==0){
            return new Response($return_json);
        }

        //获取entity_manager对象
        $em = $this->getDoctrine()->getManager();
        $sql = "SELECT p.id,p.caiitem,p.caicode FROM AppBundle:EntCai p";
        $result = $em->createQuery($sql)->getResult();
        //获取json数据
        $return_json = $common_service->apiEncode(1,'新闻栏目信息',array('news_type'=>$result),200);
        return new Response($return_json);
    }
    
    /**
     * @abstract 首页头图和API推荐接口
     * @author chenyl
     * @Route("/news_bundle/position",name="news_position")
     */
    public function newsPositionAction(Request $req){

       //将通用服务保存到对象
        $common_service = $this->get('common_service');

        //验证请求的合法性，获取返回的Json字符串

        //代码合并，再次提交远端并代码合并

        $return_json = $this->checkHeaderInfo($req);

        //将校验结果解码
        $return_obj = json_decode($return_json);
        //检查请求是否合法,不合法则返回非法信息
        if($return_obj->status==0){
            return new Response($return_json);
        }
        //检查请求合法则继续
        //获取请求的页码与大小
        $page_int = $req->get('page');
        $size_int = $req->get('size');

        //获取entity_manager对象
        $em = $this->getDoctrine()->getManager();
        //获取所有推荐新闻
        $position_arr = $em->getRepository('AppBundle:EntNews')->getNewsByIsPosition($page_int,$size_int);
        
        //获取所有头图新闻
        $head_figure_arr = $em->getRepository('AppBundle:EntNews')->getNewsByIsHeadFigure();
        //封装数据
        $return_arr = array('position_arr'=>$position_arr,
                            'head_figure_arr'=>$head_figure_arr,
                            'page'=>$page_int,
                            'size'=>$size_int
                    );

        //转换为json
        $return_json = $common_service->apiEncode(1,'新闻推荐与头图的id、title和image',$return_arr,200);

        return new Response($return_json);
    }

    /**
     * @abstract 特定新闻类型Route
     * @author songzq
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @internal param $
     * @Route("/news_bundle/caiid",name="news_caiid")
     */
    public function newsCaiidAction(Request $request){
        //验证头信息是否合法
        $_strCheckHeader=$this->checkHeaderInfo($request);
        $_objCheckHeader=json_decode($_strCheckHeader);
        if($_objCheckHeader->status==0){//验证失败
            $_strReturn=$this->get('common_service')->apiEnCode($_objCheckHeader->status,$_objCheckHeader->message,[],400);
        }else {//验证成功
            $em = $this->getDoctrine()->getManager();
            //查询语句,使用实体定义的字段
            $_strSql=  'SELECT n.id,n.smallTitle,n.image,n.title,n.description FROM AppBundle:EntNews n WHERE n.caiid = :caiid ';
            //获取分页查询数据
            $size=3;   $page=1;  //建议在parameter中配置
            $query = $em->createQuery($_strSql)->setParameter('caiid', $request->get('caiid'))
                  ->setMaxResults($size)->setFirstResult($page-1);
            $result = $query->getResult();
            $date=array(
               'newsList'=>$result,
               'page'=>$page
           );
            $_strReturn = $this->get('common_service')
                ->apiEnCode($_objCheckHeader->status,'新闻特定类型',$date, 200);
        }
        return new Response($_strReturn);
    }

    /**
     * @abstract 新闻排行Route
     * @author songzq
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @internal param $
     * @Route("/news_bundle/sort",name="news_sort")
     */
    public function newsSortAction(Request $request){
       //验证头信息是否合法
        $_strCheckHeader=$this->checkHeaderInfo($request);
        $_objCheckHeader=json_decode($_strCheckHeader);
        if($_objCheckHeader->status==0){//验证失败
           $_strReturn=$this->get('common_service')->apiEnCode($_objCheckHeader->status,$_objCheckHeader->message,[],400);
       }else{//验证成功
            //依据readCount找到排行数据
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
                'SELECT n.id,n.title,n.description
                    FROM AppBundle:EntNews n
                    ORDER BY n.readCount ASC'
            )->setMaxResults(3);
            $list = $query->getResult();
            $_strReturn=$this->get('common_service')
                ->apiEnCode($_objCheckHeader->status,$_objCheckHeader->message,$list,200);
        }
        return new Response($_strReturn);
    }

    /**
     * @abstract 新闻内容Route
     * @author zhangbin
     * @param  id news_arr  新闻id 新闻排行信息
     * @return
     * @Route("/news_bundle/content",name="news_content")
     */
    public function newsContentAction(Request $request){
        //验证header头所带的信息是否合法
        $_strCheckInfo = $this->checkHeaderInfo($request);
        $_objCheckInfo = json_decode($_strCheckInfo);
        if($_objCheckInfo->status == 0){ //header验证失败
            $_strReturn = $this->get("common_service")->apiEnCode($_objCheckInfo->status,$_objCheckInfo->message,[],400);
            return new Response($_strReturn);
        }else if($_objCheckInfo->status == 1){ //header头验证成功
           //接收排行信息和id
            $id = $request->get('id');
            $paraters = array('id'=>$id);
            //依据id找到指定新闻内容
            $em = $this->getDoctrine()->getManager();
            $sql = "select news.id,news.title,news.smallTitle,news.caiid,news.image,news.content,
            news.description,news.isPosition,news.isHeadFigure,news.isAllowcomments,news.listorder,news.createTime,news.updateTime
            ,news.status,news.readCount 
            from AppBundle:EntNews news where news.id = :id";
            $query = $em->createQuery($sql)->setParameters($paraters);
            $_arrNews = $query->getResult();
            //将指定新闻
            $_strReturn = $this->get("common_service")->apiEnCode($_objCheckInfo->status,$_objCheckInfo->message,$_arrNews,200);
        }
        return new Response($_strReturn);
    }
}