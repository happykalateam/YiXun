<?php
namespace ApiBundle\Controller;

//use ApiBundle\Controller\ApiCommonController;
use AppBundle\Controller\BaseController;
use AppBundle\Service\lib\Aes;
use AppBundle\Service\lib\ApiDataOperate;
use AppBundle\Service\lib\DBOperate;
use AppBundle\Service\lib\RedisOperate;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * 涉及到的需要登录的获取数据的请求需要继承这个类，主要完成下面的工作：
 *  1、需要登录情况下才能够查询的接口所属的类需要继承这个类
 *  2、在构造函数判定access_user_token是否存在以及合法
 *  3、返回处理的user（正常登录之后的情况下）
 * Class AuthBaseController
 * @package ApiBundle\Controller
 */
class AuthBaseController extends BaseController{

    /**
     * @var object
     */
    public $_arrUser = '';

    /**
     * @abstract 验证请求返回的信息
     * @var string
     */
    public $_strJsonInfo = '';

    /**
     * @abstract 构造函数中处理客户端登录逻辑的验证
     * AuthBase constructor.
     */
    public function __construct(){
        parent::__construct();
        if(!$this->isAppLogin()){
            $this->_strJsonInfo = ApiDataOperate::apiEnCode('0','用户尚未登录，请登录','',400);
            echo($this->_strJsonInfo);exit;
        }
    }

    /**
     * @abstract 验证客户端在发起请求的时候有没有登录
     * @return bool
     */
    function isAppLogin(){
        $_objRequest = Request::createFromGlobals();
        $_objHeader = $_objRequest->headers;
        //access-user-token为空
        if(empty($_objHeader->get('access-user-token'))){
            return false;
        }
        //不为空的情况下就需要对当前的传递过来的token进行redis存储，存储唯一的token和时间戳的凭借字符串
        $_objRedisOperate = RedisOperate::getInstance();
        $_boolPass = $_objRedisOperate->saveInfoByRedis($_objHeader->get('access-user-token'),'all_token','set');
        if(!$_boolPass){//token请求非法
//            return false;
        }
        //将传递的access-user-token解密
        $_objAes = new Aes();
        $_strTokenInfo = urldecode($_objAes->decrypt($_objHeader->get('access-user-token')));
        list($_strTime,$_strToken) = explode('&',$_strTokenInfo);
        $_arrTokenInfo = explode('=',$_strToken);
        $_strTokenInfo = $_objAes->decrypt($_arrTokenInfo[1]);
        list($_strToken,$_strID) = explode('&',$_strTokenInfo);
        $_arrTokenInfo = explode('=',$_strToken);
        //解密之后的token信息为空
        if(is_null($_arrTokenInfo[1])){
            return false;
        }
        $_objOperate = DBOperate::getInstance();
        $_arrParam = array($_arrTokenInfo[1],1);
        $users = $_objOperate->getQuery("SELECT * FROM ent_user where token = ? and status = ?",$_arrParam);
        $_arrUser = array_shift($users);
        //用户不存在，或者token过期了都是没有登录的状态
        if(is_null($_arrUser) || (count($_arrUser) == 0 && is_array($_arrUser)) ||  time() > $_arrUser['timeout']){
//            return false;
        }
        $this->_arrUser = $_arrUser;
        return true;

    }
}