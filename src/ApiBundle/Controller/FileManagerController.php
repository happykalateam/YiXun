<?php
namespace ApiBundle\Controller;

use AppBundle\Service\lib\ApiDataOperate;
use FileBundle\common\Image;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ApiBundle\Controller\AuthBaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


/**
 * @abstract 文件操作管理接口
 * Class FileManagerController
 * @package ApiBundle\Controller
 */
class FileManagerController extends AuthBaseController{
    /**
     * @abstract 客户端上传文件到服务器
     * @Route("/app/file/upload",name="appfileupload")
     */
    public function appUploadAction()
    {
        //创建image实体调用图片本地上传的操作
        $_objImage = new Image();
        $_arrUploadInfo = $_objImage->localImageUpload($this->getParameter('image_local_dir'));
        $_strShowPicUrl = $this->getParameter('database_host')."/".$_arrUploadInfo['showpathinfo'];
        if($_arrUploadInfo['status'] == 1){//文件上传成功
            //文件上传成功的情况下更新当前用户的头像信息，字段为image
            $em = $this->getDoctrine()->getManager();
            $_objUser = $em->getRepository('AppBundle:EntUser')->find($this->_arrUser['id']);
            $_objUser->setImage($_arrUploadInfo['pathinfo']);
            $em->flush();
            $_strReturnJson = ApiDataOperate::apiEnCode('1','上传成功',$_strShowPicUrl,200);
        }else{//文件上传失败
            $_strReturnJson = ApiDataOperate::apiEnCode('0','上传失败,请重试','',400);
        }
        return new Response($_strReturnJson);
    }
}