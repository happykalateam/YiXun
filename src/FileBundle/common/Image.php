<?php
namespace FileBundle\common;

//文件上传的引用部分
use Upload\Storage\FileSystem;
use Upload\File;
//二维码生成的引用部分
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;
use Symfony\Component\HttpFoundation\Response;

class Image{

    /**
     * @abstract 文件上传到本地服务器
     * @param string $_strUploadDir  上传文件的路径
     * @param string $_strFile 上传空间的name
     * @return array
     */
    public function localImageUpload($_strUploadDir = '/home/abssr/www/AbsSR/web/upload',$_strFile = 'file'){
        $objStorage = new FileSystem($_strUploadDir);
        $objFile = new File($_strFile, $objStorage);
        //将元文件的名字记录
        $_strOriginalName = $objFile->getNameWithExtension();
        // 重新给文件命名
        $_strNewFilename = uniqid();
        $objFile->setName($_strNewFilename);

        //验证上传图片的格式和大小，支持的格式类型参考 http://www.iana.org/assignments/media-types/media-types.xhtml
        $objFile->addValidations(array(
            //验证文件格式验证一种文件格式的时候直接传递'image/*',"*"替换成你的图片格式，多种格式使用数组验证
            new \Upload\Validation\Mimetype(array('image/png', 'image/gif','image/jpeg')),
            //验证文件大小不大于5M (use "B", "K", M", or "G")
            new \Upload\Validation\Size('5M')
        ));

        // Access data about the file that has been uploaded
        $arrData = array(
            'name'       => $objFile->getNameWithExtension(),
            'extension'  => $objFile->getExtension(),
            'mime'       => $objFile->getMimetype(),
            'size'       => $objFile->getSize(),
            'md5'        => $objFile->getMd5(),
            'dimensions' => $objFile->getDimensions()
        );
        //上传文件
        try {
            // Success!
            $_info = $objFile->upload();
            if($_info){//上传成功
                $arrData['status'] = 1;
                $arrData['message'] = "上传成功";
                $arrData['pathinfo'] = $_strUploadDir."/".$_strNewFilename.".".$objFile->getExtension();
                $arrData['showpathinfo'] = str_replace('/home/abssr/www','',$_strUploadDir) ."/".$_strNewFilename.".".$objFile->getExtension();
            }else{//上传失败
                $arrData = array(
                    'status' => 0,
                    'message' => '上传失败',
                );
            }
        } catch (\Exception $e) {
            // Fail!
            $errors = $objFile->getErrors();
            $arrData = array(
                'status' => 0,
                'message' => $errors,
            );
        }
        return $arrData;
    }

    /**
     * @abstract 生成二维码,该函数参数设置比较多，如果优化调整可以处理为使用数组传递参数
     * @param $__strCode  用于生成二维码的图片
     * @param $__intSize  二维码尺寸
     * @param $__strType  图片格式
     * @param $__strLabel 图片下面的label字符串
     * @param $__strLogoPath 显示在二维码中间的logo
     * @param $__logoWidth  logo的显示宽度
     * @param $__strFlag  控制输出的参数，save表示存储到目录然后response输出，output标识直接输出
     */
    public function generateQrCode($__strCode = 'hello world',$__intSize = 300,$__strType = 'png',$__intMargin = 10,$__strLabel = '请扫码',$__strLogoPath = '/home/abssr/www/AbsSR/vendor/endroid/qrcode/assets/symfony.png',$__logoWidth = 0,$__strFlag = 'output'){
        //创建二维码生成的对象
        $_objQrCode = new QrCode($__strCode);
        //设置二维码的尺寸
        $_objQrCode->setSize($__intSize);
        //设置其他的属性

        $_objQrCode
            ->setWriterByName($__strType)//输出或者存储的格式
            ->setMargin($__intMargin)//二维码部分和背景框之前的间隔
            ->setEncoding('UTF-8')//字符串格式
            ->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH)//错误修正的级别
            ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0])//二维码部分颜色
            ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255]);//背景颜色
//            ->setLabel($__strLabel, 16, '/home/abssr/www/AbsSR/vendor/endroid/qr-code/assets/noto_sans.otf', LabelAlignment::CENTER);//二维码下面的显示字符串
            if(is_file($__strLogoPath)){
                $_objQrCode->setLogoPath($__strLogoPath)//二维码中间的显示图片
                           ->setLogoWidth($__logoWidth);//logo的宽度
            }
            $_objQrCode->setValidateResult(false);
        if($__strFlag == 'output'){//直接输出二维码
            header('Content-Type: '.$_objQrCode->getContentType());
            echo $_objQrCode->writeString();exit;
        }else if($__strFlag == 'save'){//存储实际生成的二维码
            $_objQrCode->writeFile('/home/abssr/www/AbsSR/web/upload/qrcode.png');
            $response = new Response($_objQrCode->writeString(), Response::HTTP_OK, ['Content-Type' => $_objQrCode->getContentType()]);
            return $response;
        }
    }

}