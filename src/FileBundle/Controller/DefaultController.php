<?php

namespace FileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FileBundle\common\Image;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @abstract 图片文件的本地上传
     * @Route("/localupload",name="localfileupload")
     */
    public function uploadAction()
    {
        //创建image实体调用图片本地上传的操作
        $_objImage = new Image();
        $_arrUploadInfo = $_objImage->localImageUpload($this->getParameter('image_local_dir'));
        $_strReturnJson = json_encode($_arrUploadInfo);
        return new Response($_strReturnJson);
    }

    /**
     * @Route("/generateqrcode",name="generateqrcode")
     */
    public function generateQrCodeAction(Request $request)
    {
        //请求传递的参数
        $_arrRequestParam = $request->query->all();
        //实际操作生成二维码之前验证几个基础的参数是否存在code，size，type，flag,这四个参数中任何一个不存在都要给出提示
        if(!array_key_exists('code',$_arrRequestParam) || !array_key_exists('size',$_arrRequestParam) || !array_key_exists('type',$_arrRequestParam) || !array_key_exists('flag',$_arrRequestParam)){
            return new Response('请检查基础参数是否齐全，基础参数包含了code,size,type,flag');
        }
        //创建image实体调用图片本地上传的操作
        $_objImage = new Image();
        //参数赋值到具体的变量中，基础参数只要通过之前的验证就存在不用使用array_key_exit再次做验证
        $__strCode = $_arrRequestParam['code'];
        $__intSize = $_arrRequestParam['size'];
        $__strType = $_arrRequestParam['type'];
        $__strFlag = $_arrRequestParam['flag'];
        $__intMargin = '10';
        $__strLabel = '';
        //二维码下面的文字显示
        if(array_key_exists('label',$_arrRequestParam)){
            $__strLabel = $_arrRequestParam['label'];
        }else{
            $__strLabel = '';
        }
        //二维码中间的图片显示处理
        if(array_key_exists('logopath',$_arrRequestParam)){
            if( !is_null($_arrRequestParam['logopath']) && $_arrRequestParam['logopath'] == 1){
                $__strLogoPath = $this->getParameter('qrcode').'assets/symfony.png';//二维码中间显示的图片
                $__logoWidth = 50;
            }else{
                $__strLogoPath = '';
                $__logoWidth = '';
            }
        }else{
            $__strLogoPath = '';
            $__logoWidth = '';
        }
        if($__strFlag == 'output'){//直接输出
            $_objImage->generateQrCode($__strCode,$__intSize,$__strType,$__intMargin,$__strLabel,$__strLogoPath,$__logoWidth,$__strFlag);
        }else if($__strFlag == 'save'){
            $_objReturn = $_objImage->generateQrCode($__strCode,$__intSize,$__strType,$__intMargin,$__strLabel,$__strLogoPath,$__logoWidth,$__strFlag);
            return $_objReturn;
        }
    }
}
