<?php

namespace VersionBundle\Controller;

use AppBundle\Entity\EntVersion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/version/add",name="version_add")
     */
    public function indexAction(Request $request)
    {
        if($request->isMethod('post')){//页面post提交的时候存储信息
            $_strFormData = urldecode($_POST['formdata']);
            //将上述的form表单字符串分割为数组
            $_arrFormData = explode('&',$_strFormData);
            $_objEntVersion = new EntVersion();
            foreach($_arrFormData as $key=>$value){
                $_arrTemp = explode('=',$value);
                $_strMethod = 'set'.$_arrTemp[0];//拼接页面属性所对应的set函数
                if(method_exists($_objEntVersion,$_strMethod)){//对应的方法在对象中是存在的则调用这个方法
                    $_objEntVersion->$_strMethod($_arrTemp[1]);
                }
            }
            //设置状态值和创建时间更新时间
            $_objEntVersion->setStatus(1);
            $_objDate = new \DateTime('now');
            $_objEntVersion->setCreateTime($_objDate);
            //数据存储
            $em = $this->getDoctrine()->getManager();
            $em->persist($_objEntVersion);
            $em->flush();
            echo $_objEntVersion->getId();exit;
        }else{
            return $this->render('VersionBundle:Default:add.html.twig');
        }
    }

    /**
     * @Route("/version/list",name="version_list")
     */
    public function listAction(Request $request)
    {
        //分页数据查询
        $objEntityManager = $this->get('doctrine.orm.entity_manager');//获取实体管理器
        $arrCondition = $_POST;  //保存查询条件的数组
        $strSql   = "SELECT version FROM AppBundle:EntVersion version";//创建查询sql
        $sql_arr = $objEntityManager->getRepository("AppBundle:EntVersion")->getDataByConitionForVersion($strSql,$arrCondition);
        $sql = $sql_arr['sql'];  //获取查询sql
        unset($sql_arr['sql']);  //只保留查询条件
        $query = $objEntityManager->createQuery($sql)->setParameters($sql_arr);
        $paginator  = $this->get('knp_paginator');
        //第一个参数为查询的sql创建的Query 第二个参数表示从第几页开始查询，第三个参数标识每页的查询数量
        $pagination = $paginator
            ->paginate($query, $request->query->getInt('page', 1), 3);
        //获取其中的item数据将其中的data类型处理成字符串形式
        $_arrItems = $pagination->getItems();
        $_arrItemNum = array();
        $intNum = 1;
        foreach($_arrItems as $key=>$value){
            $_objDate = $value->getCreatetime();
            if(is_object($_objDate)){
                $value->setCreatetime($_objDate->format('Y-m-d H:i:s'));
            }
            $_arrItemNum[$value->getID()] = $intNum++;
        }
        $pagination->setItems($_arrItems);

        return $this->render('VersionBundle:Default:index.html.twig', array('pagination' => $pagination,'totaldatanum' => $pagination->getTotalCount()));

    }
}
