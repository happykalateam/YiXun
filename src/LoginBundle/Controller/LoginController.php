<?php

namespace LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\Session\Session;

use Gregwar\CaptchaBundle\Type\CaptchaType;
use Gregwar\Captcha\CaptchaBuilder;
use Gregwar\Captcha\PhraseBuilder;


class LoginController extends Controller
{
    //登录页面
    /**
     * @Route("/login",name="login")
     */
    public function loginAction()
    {
        return $this->render('LoginBundle:Login:login.html.twig');
    }

    //验证当前的登录用户
    /**
     * @Route("/logincheck",name="login_check")
     */
    public function checkAction(Request $request)
    {
        if($request->isMethod('post')){//是post请求的情况下
            //首先校验验证码有没有输入正确
            //获取session
            $_objSession = $request->getSession();
            if($request->get('code') != $_objSession->get('captcha')){//输入的验证码实际的验证码不一致
                return $this->render('LoginBundle:Login:login.html.twig');
//                return new Response('验证码不正确');
            }
            $_objRepository = $this->getDoctrine()->getRepository('AppBundle:EntAdminUser');
            //做数据库查询的时候
            try{
                $_objAdminUser = $_objRepository->findOneByUsername(array('username' => $request->get('username')));
            }catch(\Exception $e){
                return new Response($e->getMessage());
            }
            //判断用户是否存在
            if(!$_objAdminUser || $_objAdminUser->getStatus() != $this->getParameter('userstatus_normal')){
                return new Response('该用户不存在或者已经失效');
            }
            //判断输入的密码是否正确
            if($_objAdminUser->getPassword() != $request->get('password')){
                return new Response('该用户不存在或者已经失效');
            }

            //正常登录的情况下，需要更新当前用户的登录时间和登录ip，并将用户信息存储到session中
            $_objAdminUser->setLastLoginDate(new \DateTime('now'));
            $_objAdminUser->setLastLoginIp($request->getClientIp());
            $em = $this->getDoctrine()->getManager();
            $em->persist($_objAdminUser);
            $em->flush();

            $_objSession->set($this->getParameter('user_session_scope'),$_objAdminUser);
            //跳转到管理员用户展示页面
            return $this->redirect('/AbsSR/web/app_dev.php');
        }else{
            return new Response('请求不合法');
        }
    }


    /**
     * @Route("/loginout",name="loginout")
     */
    public function loginoutAction(Request $request)
    {
        //销毁登录的session
        $_objSession = $request->getSession();
        $_objSession->set($this->getParameter('user_session_scope'),'');
        return $this->redirectToRoute('login');
    }

    /**
     * @Route("/captcha/code",name="captchacode")
     */
    public function captcha(Request $request)
    {
        $intWidth =  !is_null($this->getParameter('captcha_width'))?$this->getParameter('captcha_width'):150;// 验证码图片的宽度
        $intHeight = !is_null($this->getParameter('captcha_height'))?$this->getParameter('captcha_height'):50;  // 验证码图片的高度
        $intLength = !is_null($this->getParameter('captcha_length'))?$this->getParameter('captcha_length'):50;;  // 验证码字符的长度
        $boolNoeffect = false;  // 是否忽略验证图片上的干扰线条
        //根据给定的字符串长度去创建验证码的图片
        $objPharse = new PhraseBuilder();
        $objCaptcha = new CaptchaBuilder($objPharse->build($intLength));
        $image = $objCaptcha->setIgnoreAllEffects($boolNoeffect)->build($intWidth, $intHeight)->get();
        //获取session将生成的验证码字符串存储到session中
        $session = $request->getSession();
        $session->set('captcha', $objCaptcha->getPhrase());
        $res = new Response($image);
        $res->headers->set('content-type', 'image/jpeg');
        return $res;
    }

}
