/*
Navicat PGSQL Data Transfer

Source Server         : 192.168.0.188
Source Server Version : 90506
Source Host           : 192.168.0.188:5432
Source Database       : abssrtemp
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90506
File Encoding         : 65001

Date: 2018-02-07 09:10:53
*/


-- ----------------------------
-- Sequence structure for ent_active_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ent_active_id_seq";
CREATE SEQUENCE "public"."ent_active_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9999999999999
 START 1
 CACHE 1;
SELECT setval('"public"."ent_active_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for ent_admin_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ent_admin_user_id_seq";
CREATE SEQUENCE "public"."ent_admin_user_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 10000000
 START 6
 CACHE 1;
SELECT setval('"public"."ent_admin_user_id_seq"', 6, true);

-- ----------------------------
-- Sequence structure for ent_app_active_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ent_app_active_id_seq";
CREATE SEQUENCE "public"."ent_app_active_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for ent_cai_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ent_cai_id_seq";
CREATE SEQUENCE "public"."ent_cai_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for ent_comment_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ent_comment_id_seq";
CREATE SEQUENCE "public"."ent_comment_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for ent_crab_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ent_crab_id_seq";
CREATE SEQUENCE "public"."ent_crab_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for ent_news_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ent_news_id_seq";
CREATE SEQUENCE "public"."ent_news_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 1000000000000000
 START 9
 CACHE 1;
SELECT setval('"public"."ent_news_id_seq"', 9, true);

-- ----------------------------
-- Sequence structure for ent_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ent_user_id_seq";
CREATE SEQUENCE "public"."ent_user_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for ent_user_news_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ent_user_news_id_seq";
CREATE SEQUENCE "public"."ent_user_news_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for ent_version_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."ent_version_id_seq";
CREATE SEQUENCE "public"."ent_version_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 8
 CACHE 1;
SELECT setval('"public"."ent_version_id_seq"', 8, true);

-- ----------------------------
-- Table structure for ent_active
-- ----------------------------
DROP TABLE IF EXISTS "public"."ent_active";
CREATE TABLE "public"."ent_active" (
"id" int4 NOT NULL,
"version" varchar(255) COLLATE "default" DEFAULT NULL::character varying,
"did" varchar(255) COLLATE "default" DEFAULT NULL::character varying,
"model" varchar(255) COLLATE "default" DEFAULT NULL::character varying,
"createtime" int4
)
WITH (OIDS=FALSE)

;
COMMENT ON COLUMN "public"."ent_active"."version" IS '客户端版本';
COMMENT ON COLUMN "public"."ent_active"."did" IS '设备号';
COMMENT ON COLUMN "public"."ent_active"."model" IS '设备型号';
COMMENT ON COLUMN "public"."ent_active"."createtime" IS '初始化时间';

-- ----------------------------
-- Records of ent_active
-- ----------------------------
INSERT INTO "public"."ent_active" VALUES ('1', '1', '1111', 'xiaomi', '1517402498');

-- ----------------------------
-- Table structure for ent_admin_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."ent_admin_user";
CREATE TABLE "public"."ent_admin_user" (
"id" int4 NOT NULL,
"username" varchar(254) COLLATE "default",
"password" varchar(254) COLLATE "default",
"qq" varchar(254) COLLATE "default",
"weixin" varchar(254) COLLATE "default",
"telephone" varchar(254) COLLATE "default",
"email" varchar(254) COLLATE "default",
"last_login_date" date,
"last_login_ip" varchar(254) COLLATE "default",
"listorder" int4,
"status" int4,
"create_time" varchar(254) COLLATE "default",
"update_time" varchar(254) COLLATE "default"
)
WITH (OIDS=FALSE)

;
COMMENT ON TABLE "public"."ent_admin_user" IS '后台用户表';

-- ----------------------------
-- Records of ent_admin_user
-- ----------------------------
INSERT INTO "public"."ent_admin_user" VALUES ('1', 'happykala', 'happykala', '1030775250', 'happykala18627867192', '18963970962', '1030775250@qq.com', '2018-01-30', '192.168.0.125', '1', '1', '1514881252', '1514881252');
INSERT INTO "public"."ent_admin_user" VALUES ('2', 'huxiongwei', 'huxiongwei', '1030775250', '18627867192', '18963970962', '1030775250@qq.com', null, null, '2', '1', '1514881365', '1514881365');
INSERT INTO "public"."ent_admin_user" VALUES ('3', 'huxiongwei', 'huxiongwei', '1030775250', '18627867192', '18963970962', '1030775250@qq.com', null, null, '3', '1', '1514881392', '1514881392');
INSERT INTO "public"."ent_admin_user" VALUES ('4', 'lihua', 'lihua', '', '', '', '', null, null, '4', '1', '1514883455', '1514883455');
INSERT INTO "public"."ent_admin_user" VALUES ('5', '123', '123', '123', '123', '123', '123', null, null, '1', '1', '1515653408', '1515653408');
INSERT INTO "public"."ent_admin_user" VALUES ('6', '444', '444', '444', '444', '444', '444', null, null, '1', '1', '1515653431', '1515653431');

-- ----------------------------
-- Table structure for ent_app_active
-- ----------------------------
DROP TABLE IF EXISTS "public"."ent_app_active";
CREATE TABLE "public"."ent_app_active" (
"id" int4 NOT NULL,
"ent_id" int4
)
WITH (OIDS=FALSE)

;
COMMENT ON TABLE "public"."ent_app_active" IS 'app启动表
';

-- ----------------------------
-- Records of ent_app_active
-- ----------------------------

-- ----------------------------
-- Table structure for ent_cai
-- ----------------------------
DROP TABLE IF EXISTS "public"."ent_cai";
CREATE TABLE "public"."ent_cai" (
"id" int4 NOT NULL,
"caiitem" varchar(255) COLLATE "default" NOT NULL,
"caicode" varchar(255) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of ent_cai
-- ----------------------------
INSERT INTO "public"."ent_cai" VALUES ('1', '首页', 'firstpage');
INSERT INTO "public"."ent_cai" VALUES ('2', '时政', 'politics');
INSERT INTO "public"."ent_cai" VALUES ('3', '体育', 'sport');
INSERT INTO "public"."ent_cai" VALUES ('4', '娱乐', 'entertainment');
INSERT INTO "public"."ent_cai" VALUES ('5', '军事', 'militory');
INSERT INTO "public"."ent_cai" VALUES ('6', '国际', 'internation');
INSERT INTO "public"."ent_cai" VALUES ('7', '健康', 'health');
INSERT INTO "public"."ent_cai" VALUES ('8', '房产', 'house');

-- ----------------------------
-- Table structure for ent_comment
-- ----------------------------
DROP TABLE IF EXISTS "public"."ent_comment";
CREATE TABLE "public"."ent_comment" (
"id" int4 NOT NULL,
"ent_id" int4,
"ent_id2" int4
)
WITH (OIDS=FALSE)

;
COMMENT ON TABLE "public"."ent_comment" IS '评论表';

-- ----------------------------
-- Records of ent_comment
-- ----------------------------

-- ----------------------------
-- Table structure for ent_crab
-- ----------------------------
DROP TABLE IF EXISTS "public"."ent_crab";
CREATE TABLE "public"."ent_crab" (
"id" int4 NOT NULL,
"ent_id" int4
)
WITH (OIDS=FALSE)

;
COMMENT ON TABLE "public"."ent_crab" IS 'APP异常性能数据表';

-- ----------------------------
-- Records of ent_crab
-- ----------------------------

-- ----------------------------
-- Table structure for ent_news
-- ----------------------------
DROP TABLE IF EXISTS "public"."ent_news";
CREATE TABLE "public"."ent_news" (
"id" int4 NOT NULL,
"ent_id" int4,
"title" varchar(254) COLLATE "default",
"small_title" varchar(254) COLLATE "default",
"caiid" int4,
"image" varchar(254) COLLATE "default",
"content" text COLLATE "default",
"description" varchar(254) COLLATE "default",
"is_position" int4,
"is_head_figure" int4,
"is_allowcomments" int4,
"listorder" int4,
"source_type" int4,
"create_time" varchar(254) COLLATE "default",
"update_time" varchar(254) COLLATE "default",
"status" int4,
"read_count" int4
)
WITH (OIDS=FALSE)

;
COMMENT ON TABLE "public"."ent_news" IS '娱乐新闻列表
';

-- ----------------------------
-- Records of ent_news
-- ----------------------------
INSERT INTO "public"."ent_news" VALUES ('1', null, '测试标题', '测试标题', '1', '/AbsSR/web/upload/5a6047a15d5cc.png', '<p>奥术大师大所多</p>', '啊飒飒大师大师', '1', '1', '1', '1', null, '1516259452', '1516259452', '1', '5');
INSERT INTO "public"."ent_news" VALUES ('2', null, '吉林原常务副省长田学仁被提请减刑', '老虎减刑', '1', '/AbsSR/web/upload/5a6844805c88b.png', '<p style="margin-top: 0px; margin-bottom: 25px; padding: 0px; text-indent: 28px; font-size: 14px; text-align: justify; word-wrap: break-word; word-break: normal; color: rgb(43, 43, 43); font-family: simsun, arial, helvetica, clean, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);">2018年1月11日，刑罚执行机关以罪犯田学仁能认罪悔罪，自觉接受教育改造，积极参加思想文化教育活动，按时完成劳动任务，多次获得监狱奖励为由，建议将其刑罚减为有期徒刑，并依法对其剥夺政治权利终身的附加刑予以酌减。</p><p style="margin-top: 0px; margin-bottom: 25px; padding: 0px; text-indent: 28px; font-size: 14px; text-align: justify; word-wrap: break-word; word-break: normal; color: rgb(43, 43, 43); font-family: simsun, arial, helvetica, clean, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);">公示期限为五日，即自2018年1月25日起至2018年1月29日止。凡对该罪犯减刑有意见者，请于公示期限内向该院提交书面意见，寄至审监庭，或拨打电话010-85268569举报。</p><p style="margin-top: 0px; margin-bottom: 25px; padding: 0px; text-indent: 28px; font-size: 14px; text-align: justify; word-wrap: break-word; word-break: normal; color: rgb(43, 43, 43); font-family: simsun, arial, helvetica, clean, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);">公开资料显示，出生于1947年3月田学仁是辽宁人，却长期在吉林任职。47岁时，其升任长春市委副书记，4年后改任吉林市委书记，并于2001年1月升为吉林省委常委、延边自治州委书记。</p><p style="margin-top: 0px; margin-bottom: 25px; padding: 0px; text-indent: 28px; font-size: 14px; text-align: justify; word-wrap: break-word; word-break: normal; color: rgb(43, 43, 43); font-family: simsun, arial, helvetica, clean, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);">2004年12月，田学仁任吉林省委常委、常务副省长，3年后“去常”，任吉林银行党委书记、董事长。2011年11月，中纪委证实，田学仁因涉嫌严重违纪，正接受组织调查。半年后，此人被双开。问题通报显示，他利用职务上的便利为他人谋取利益，收受巨额钱物；收受礼金。</p><p><br/></p>', '吉林原常务副省长田学仁被提请减刑', '1', '1', null, '1', null, '1516782785', '1516782785', '1', null);
INSERT INTO "public"."ent_news" VALUES ('3', null, '台胞成为上海市政协委员 台官方：已注销其户籍', '台胞，政协委员', '2', '/AbsSR/web/upload/5a68cdf1b3f4d.png', '<p style="margin-top: 22px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);">海外网1月25日消息，台湾出生、上海长大的谢国群成为政协上海市第十三届委员会的一名政协委员。对此，台当局陆委会称，谢国群在中国大陆设籍超过10年，身份是中国大陆人士，台当局早在2007年即已注销谢国群的户籍，当事人早已不具台湾人身份，并非“两岸人民关系条例”第33条规范对象。</p><p style="margin-top: 22px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);">台胞成为上海市政协委员台当局称已注销其户籍</p><p style="margin-top: 22px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);">据新华社报道，22日，新任上海市政协委员、上海中医药大学附属岳阳中西医结合医院肿瘤科主治医师谢国群出席上海市政协会议的开幕式。谢国群出生在台湾彰化，从小他的父亲就有一个“乒乓梦”，所以在两岸恢复交流后将其送到上海一边读书一边练球。就这样，他在上海南洋模范中学完成高中教育，随后考取上海交通大学工业外贸系，本科毕业后转考上海中医药大学中医内科学专业。</p><p style="margin-top: 22px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);">据报道，谢国群成为上海市政协委员，改变了以往来自台盟上海市委、上海市台湾同胞联谊会的委员人选基本是“大陆出生、大陆成长的台胞二代”的现象。这名上世纪90年代来大陆求学的台生，已经取得大陆的居民身份证，在以台湾省籍当选的上海市人大代表和协商推荐产生的政协委员中，尚属首例。</p><p style="margin-top: 22px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);">对此，台媒24日报道称，台当局陆委会又老调重弹称大陆“统战”。据亲绿台媒报道，陆委会称，大陆“早期以1949年前赴陆台籍人士及其二代，作为对台‘统战’重点对象，近来则企图以复制‘卢丽安’模式，积极笼络在台湾出生，赴陆发展的台籍人士为宣传样版。”陆委会还叫嚣称，“这种作法无法获得台湾人民认同，也无法代表台湾的主流民意”，只是“徒劳与不得台湾人心的作法”。</p><p style="margin-top: 22px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);">国台办曾表示：大陆和台湾都是台湾省籍同胞的家</p><p style="margin-top: 22px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);">这已经不是台当局陆委会近期第一次进行类似回应。十九大台籍代表卢丽安和她的家人，日前也曾被台湾方面注销了户籍。对此，国台办发言人马晓光在2017年11月15日举行的新闻发布会上则表示，无论她在台湾是否有户籍，大陆和台湾都是她的家。</p><p style="margin-top: 22px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);">马晓光表示，卢丽安教授是土生土长的台湾省籍同胞，她通过自己的观察和思考，作出了加入中国共产党的选择。她作为党代表出席十九大，在接受记者采访时表示，以台湾的女儿为荣、以生为中国人为傲；希望台湾同胞和岛内乡亲参与共圆中国梦的伟大事业。她还说，人心的距离以及一些误会是可以消解的。她的这些看法得到了两岸同胞的广泛认同和肯定，台湾方面无论采取什么措施，都无损她爱家乡、爱祖国大陆的情怀。无论她在台湾是否有户籍，大陆和台湾都是她的家。</p><p style="margin-top: 22px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);">台胞证改18位有望今年落实？国台办：抓紧研究</p><p style="margin-top: 22px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);">此外，就在1月17日的发布会上，有记者提问，大陆之前有表明说要出台研究便利台胞在大陆生活的一些政策措施，其中包括研究台胞证要改18位，请问这项措施的调查进度，有可能在2018年落实吗？</p><p style="margin-top: 22px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255);">马晓光表示，大家也都知道，逐步为在大陆学习、创业、就业、生活的台湾同胞提供与大陆同胞同等的待遇，是我们的政策措施，写进了十九大报告。在去年采取的二十多项政策措施基础上，我们今年还会会同有关部门持续推出新的政策措施。至于你提到的台湾同胞持台胞证在大陆享受一些公共服务遇到的不方便的问题，据我了解，有关部门正在抓紧汇总研究，有确切的消息，我们会及时公布。</p><p><br/></p>', '台胞，政协委员', '1', '1', null, '1', null, '1516817931', '1516817931', '1', null);
INSERT INTO "public"."ent_news" VALUES ('4', null, '不怕中俄联手的美国，为啥管不住北约小弟土耳其', '不怕中俄联手的美国，为啥管不住北约小弟土耳其', '2', '/AbsSR/web/upload/5a68cf3c1c500.png', '<p><span style="color: rgb(51, 51, 51); font-family: arial; text-align: justify; background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;土耳其作为北约国家，实际上是一个具有“特别”色彩的成员国。先从土耳其的武器装备构成说起，土耳其空军装备有欧洲联合研发的A400M军事运输及，以色列产苍鹭无人机，美制MQ-1无人机，F-16战斗机，苏联制米-8运输机；土耳其陆军则装备有韩国造K9自行榴弹炮，苏联造BTR-80运输车，德国豹2主战坦克等武器，海军主要的装备都来自美德。最重要的是，土耳其还要装备俄制S-400防空导弹。可见，土耳其的武器组成不但买了欧洲的，美国的也有苏俄的。怎么购买武器，可以看出一个国家的心态。从土耳其购武对象就可以判断，这是一个极为敏感的国家，而且喜欢在各大势力间刷存在感</span></p><p><span style="color: rgb(51, 51, 51); font-family: arial; text-align: justify; background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: rgb(51, 51, 51); font-family: arial; text-align: justify; background-color: rgb(255, 255, 255);">实际上土耳其和苏俄打过很对次仗，但是因为利益相同也有走近的时候。土耳其作为地跨亚欧两大洲的国家，不但与诸多中东国家领土接壤，而且扼守土耳其海峡，是黑海通往地中海乃至大西洋的咽喉地带。历史上对于土耳其海峡的争夺，参与进来的国家不但有苏联也有英法等国。</span></span></p>', '不怕中俄联手的美国，为啥管不住北约小弟土耳其', '1', '1', null, '1', null, '1516818276', '1516818276', '1', null);
INSERT INTO "public"."ent_news" VALUES ('5', null, '2017年江苏十大体育新闻揭晓 女排双冠排名第三', '2017年江苏十大体育新闻揭晓 女排双冠排名第三', '3', '/AbsSR/web/upload/5a68d0741d8ec.png', '<p style="margin-top: 0px; margin-bottom: 30px; padding: 0px; text-size-adjust: 100%; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, 微软雅黑, SimSun, 宋体, Arial; font-size: 18px; letter-spacing: 1px; white-space: normal;">扬子晚报讯（通讯员 徐邦俊） 1月22日，由省体育局、省体育记者协会联合主办的2017年度江苏十大体育新闻评选结果在宁揭晓，分别是：</p><p style="margin-top: 0px; margin-bottom: 30px; padding: 0px; text-size-adjust: 100%; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, 微软雅黑, SimSun, 宋体, Arial; font-size: 18px; letter-spacing: 1px; white-space: normal;">　　1、省体育局党组中心组成员集体到镇江市丹徒区世业镇开展主题学习调研。</p><p style="margin-top: 0px; margin-bottom: 30px; padding: 0px; text-size-adjust: 100%; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, 微软雅黑, SimSun, 宋体, Arial; font-size: 18px; letter-spacing: 1px; white-space: normal;">　　2、江苏省在第十三届全运会上实现运动成绩与精神文明双丰收，群体项目大放异彩。</p><p style="margin-top: 0px; margin-bottom: 30px; padding: 0px; text-size-adjust: 100%; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, 微软雅黑, SimSun, 宋体, Arial; font-size: 18px; letter-spacing: 1px; white-space: normal;">　　3、江苏女排首夺全国联赛冠军、全运会冠军。</p><p style="margin-top: 0px; margin-bottom: 30px; padding: 0px; text-size-adjust: 100%; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, 微软雅黑, SimSun, 宋体, Arial; font-size: 18px; letter-spacing: 1px; white-space: normal;">　　4、江苏体育惠民举措频出，十三五期间将建成1000个体育公园，仅2017年发放体育消费券5000万元。</p><p style="margin-top: 0px; margin-bottom: 30px; padding: 0px; text-size-adjust: 100%; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, 微软雅黑, SimSun, 宋体, Arial; font-size: 18px; letter-spacing: 1px; white-space: normal;">　　5、省体育局与体育总局冬运中心签署冬季运动项目协议，南方省份将有望迎来冰雪项目。</p><p style="margin-top: 0px; margin-bottom: 30px; padding: 0px; text-size-adjust: 100%; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, 微软雅黑, SimSun, 宋体, Arial; font-size: 18px; letter-spacing: 1px; white-space: normal;">　　6、2017江苏广场舞大赛完美收官，现场3000人一起舞动。</p><p style="margin-top: 0px; margin-bottom: 30px; padding: 0px; text-size-adjust: 100%; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, 微软雅黑, SimSun, 宋体, Arial; font-size: 18px; letter-spacing: 1px; white-space: normal;">　　7、江苏省政府办公厅出台《江苏省足球改革发展实施意见》，省体育局印发《江苏足球“十三五”发展规划》，江苏足球改革全面启动。</p><p style="margin-top: 0px; margin-bottom: 30px; padding: 0px; text-size-adjust: 100%; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, 微软雅黑, SimSun, 宋体, Arial; font-size: 18px; letter-spacing: 1px; white-space: normal;">　　8、江苏体育产业系列政策密集发布，小镇建设方兴未艾，智慧体育蓬勃发展。</p><p style="margin-top: 0px; margin-bottom: 30px; padding: 0px; text-size-adjust: 100%; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, 微软雅黑, SimSun, 宋体, Arial; font-size: 18px; letter-spacing: 1px; white-space: normal;">　　9、2017年江苏体彩销量位列全国第一，成为全国首个销量突破200亿大关的省份。</p><p style="margin-top: 0px; margin-bottom: 30px; padding: 0px; text-size-adjust: 100%; color: rgb(77, 79, 83); font-family: &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, 微软雅黑, SimSun, 宋体, Arial; font-size: 18px; letter-spacing: 1px; white-space: normal;">　　10、江苏运动员频频站上最高领奖台，於之莹创造“前无古人”32连胜纪录，赵帅成为中国第一位男子跆拳道世锦赛冠军得主。</p><p><br/></p>', '2017年江苏十大体育新闻揭晓 女排双冠排名第三', '1', null, null, '1', null, '1516818603', '1516818603', '1', null);
INSERT INTO "public"."ent_news" VALUES ('6', null, '冬日里如何正确晨练？', '冬日里如何正确晨练？', '3', '/AbsSR/web/upload/5a68d176993bd.png', '<p style="margin-top: 0px; margin-bottom: 14px; padding: 0px; line-height: 1.7; text-indent: 2em; color: rgb(51, 51, 51); font-size: 14px; font-family: 宋体, &quot;Arial Narrow&quot;, arial, serif; white-space: normal; background-color: rgb(255, 255, 255);">连日来，苏城的气温持续下降，不过众多热爱运动的市民，健身热情不减。昨日，记者在相门城墙广场看到，不少市民仍在坚持晨练。寒冷的冬季，老年人锻炼身体应该注意些什么呢？哪些项目适合在冬季开展呢？</p><p style="margin-top: 0px; margin-bottom: 14px; padding: 0px; line-height: 1.7; text-indent: 2em; color: rgb(51, 51, 51); font-size: 14px; font-family: 宋体, &quot;Arial Narrow&quot;, arial, serif; white-space: normal; background-color: rgb(255, 255, 255);">注意防冻和感冒冬季晨练最好在阳光下进行</p><p style="margin-top: 0px; margin-bottom: 14px; padding: 0px; line-height: 1.7; text-indent: 2em; color: rgb(51, 51, 51); font-size: 14px; font-family: 宋体, &quot;Arial Narrow&quot;, arial, serif; white-space: normal; background-color: rgb(255, 255, 255);">近年，随着太极拳、健身气功的普及，苏州市武术协会有超过千名会员成为二级以上社会体育指导员。他们活跃在街头巷尾，成为各个晨晚练点的健身带头人。65岁的沈建华就是其中一名国家级社会体育指导员，她常年在全民健身大课堂相门城墙广场免费教学点授课，为市民指导健身操、健身气功和太极拳等项目。</p><p style="margin-top: 0px; margin-bottom: 14px; padding: 0px; line-height: 1.7; text-indent: 2em; color: rgb(51, 51, 51); font-size: 14px; font-family: 宋体, &quot;Arial Narrow&quot;, arial, serif; white-space: normal; background-color: rgb(255, 255, 255);">她告诉记者，每天前来相门城墙广场晨练的人群大概有100人左右，虽说太极拳、健身气功等运动强度不大，但健身的效果很明显。尤其是在冬季，气候寒冷，爆发性的无氧运动容易引起身体不适，所以，晨练可以选择太极这类动作幅度较小、热量消耗较大的有氧运动。</p><p style="margin-top: 0px; margin-bottom: 14px; padding: 0px; line-height: 1.7; text-indent: 2em; color: rgb(51, 51, 51); font-size: 14px; font-family: 宋体, &quot;Arial Narrow&quot;, arial, serif; white-space: normal; background-color: rgb(255, 255, 255);">但是，不同的季节，晨练的具体时间、地点完全有着极大的不同。“春夏季要求在阴凉处活动，以避免中暑，而冬季则强调在阳光下运动，不要在风口进行晨练，以防寒气入侵，时间一般应选在7点以后。”沈建华建议，早上出来锻炼的时候，可以适当吃些东西，增加热量。服装上穿保暖内衣和外套就可以，锻炼时把外套脱掉，打完拳后就把外套穿上，以防感冒。而且，打拳时最好戴着手套，这样可以保护手不被冻伤。</p><p style="margin-top: 0px; margin-bottom: 14px; padding: 0px; line-height: 1.7; text-indent: 2em; color: rgb(51, 51, 51); font-size: 14px; font-family: 宋体, &quot;Arial Narrow&quot;, arial, serif; white-space: normal; background-color: rgb(255, 255, 255);">不要练到大汗淋漓锻炼前可利用广播操热身</p><p style="margin-top: 0px; margin-bottom: 14px; padding: 0px; line-height: 1.7; text-indent: 2em; color: rgb(51, 51, 51); font-size: 14px; font-family: 宋体, &quot;Arial Narrow&quot;, arial, serif; white-space: normal; background-color: rgb(255, 255, 255);">由于冬季气温较低，人们在锻炼的时候非常容易受伤。因此，每次锻炼前一定要注意做好充分的准备活动。“如果打一个小时的拳，热身时间不能低于半小时。”沈建华说，一般我会建议大家先跳一套广播操，使身体热起来之后再开始练习太极拳。而且打拳最好以微汗为最好，不宜练到大汗淋漓。</p><p style="margin-top: 0px; margin-bottom: 14px; padding: 0px; line-height: 1.7; text-indent: 2em; color: rgb(51, 51, 51); font-size: 14px; font-family: 宋体, &quot;Arial Narrow&quot;, arial, serif; white-space: normal; background-color: rgb(255, 255, 255);">对于刚刚开始接触太极拳的爱好者，沈建华建议，最好是从简单的入手，即先学简化的太极拳，经过一段时间的练习以后，掌握了动作要领，再开始学习传统的太极拳。由于太极拳特有的运动形式是膝关节始终处于半蹲姿势，下肢运动负荷比较大，长时间、低重心的运动，很容易造成膝关节软骨、韧带损伤。因此，在打拳过程中，不要过分追求“太极姿势”的低重心行拳姿势。</p><p style="margin-top: 0px; margin-bottom: 14px; padding: 0px; line-height: 1.7; text-indent: 2em; color: rgb(51, 51, 51); font-size: 14px; font-family: 宋体, &quot;Arial Narrow&quot;, arial, serif; white-space: normal; background-color: rgb(255, 255, 255);">练太极拳很适合老年人市区10个教学点免费授课</p><p style="margin-top: 0px; margin-bottom: 14px; padding: 0px; line-height: 1.7; text-indent: 2em; color: rgb(51, 51, 51); font-size: 14px; font-family: 宋体, &quot;Arial Narrow&quot;, arial, serif; white-space: normal; background-color: rgb(255, 255, 255);">对每个人而言，运动总比不运动好。只要你能够坚持练习太极拳那么就能够从中获益。据了解，练太极拳可以通过人们对身体平衡的控制，从而抑制大脑皮层中过多的兴奋点，令大脑皮层内的兴奋点集中于某一区域，从而令身心进入专一的状态，做到修身养性。</p><p style="margin-top: 0px; margin-bottom: 14px; padding: 0px; line-height: 1.7; text-indent: 2em; color: rgb(51, 51, 51); font-size: 14px; font-family: 宋体, &quot;Arial Narrow&quot;, arial, serif; white-space: normal; background-color: rgb(255, 255, 255);">改善人体新陈代谢，也是练习太极拳的好处之一。老年人的很多疾病是与新陈代谢的降低分不开的。因此，坚持打太极拳，对降低血液胆固醇含量，预防动脉硬化有良好的作用。太极拳由于要求深长均匀的自然呼吸，且要气沉丹田，呼吸的效果增加，也就更好地加速了血液与淋巴的循环，为预防心脏各种疾病及动脉硬化建立了良好的条件。“我们很多的太极拳爱好者，在坚持运动之后，糖尿病、高血压等疾病都到了改善。”沈建华说。</p><p style="margin-top: 0px; margin-bottom: 14px; padding: 0px; line-height: 1.7; text-indent: 2em; color: rgb(51, 51, 51); font-size: 14px; font-family: 宋体, &quot;Arial Narrow&quot;, arial, serif; white-space: normal; background-color: rgb(255, 255, 255);">对于如今流行的视频教学，沈建华说：“照着一些练习太极拳的视频、图片模仿，只能学到表面的东西，收效甚微甚至适得其反。”据悉，苏州市体育局“全民健身系列大课堂”活动自2011年12月开展以来，已在市区设立了10个免费授课点，包括相门城墙广场、桂花公园等地，沈建华期待更多市民加入到全民健身的大家庭。</p><p><br/></p>', '冬日里如何正确晨练？', null, null, null, '1', null, '1516818814', '1516818814', '1', null);
INSERT INTO "public"."ent_news" VALUES ('7', null, '潘粤明腊八晒雪景 单手托腮帅气侧颜不减当年', '潘粤明腊八晒雪景 单手托腮帅气侧颜不减当年', '4', '/AbsSR/web/upload/5a68d20024295.png', '<p style="margin-top: 0px; margin-bottom: 0px; padding: 0px 0px 15px; font-family: 微软雅黑, 黑体; font-size: 18px; white-space: normal;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1月24日是阴历的腊月初八，也是传统的腊八节，也就是说从这天开始，就快要过年了。潘粤明在腊八节晒帅照，并称：“腊月初八，今天又下雪了…安！”照片中，潘粤明身穿焦糖色高领毛衣，坐在桌子前，一手撑着下巴，眼睛望向远方做沉思状，非常的帅气，远处山上白茫茫的一片。</p><p style="margin-top: 0px; margin-bottom: 0px; padding: 0px 0px 15px; font-family: 微软雅黑, 黑体; font-size: 18px; white-space: normal;">　　网友看到此微博后纷纷围观并留言称：“潘老师，腊八快乐。”“哥哥在国外工作有喝腊八粥吗，给您送上一碗，过了腊八就是年啦！</p><p><br/></p>', '潘粤明腊八晒雪景 单手托腮帅气侧颜不减当年', null, null, null, '1', null, '1516818970', '1516818970', '1', null);
INSERT INTO "public"."ent_news" VALUES ('8', null, '2018巴黎时装周新娱儿打造娱乐潮流新格局', '2018巴黎时装周新娱儿打造娱乐潮流新格局', '4', '/AbsSR/web/upload/5a68d277eaf3b.png', '<p style="box-sizing: border-box; white-space: normal; background-color: rgb(255, 255, 255); color: rgb(51, 51, 51); font-family: 微软雅黑, Helvetica, STHeiTi, sans-seri; text-indent: 2em;">近年来大牌明星扎堆出现在国际时装周的大秀现场，内地明星走入国际本是双赢之举，却往往由于各种原因难以获得交口称赞。此次巴黎时装周，新娱儿×唱吧带领时尚ICON罗云熙、孔垂楠受邀出席Y/PROJECT、山本耀司Yohji、Y-3共三场巴黎时装周秋冬大秀。作为新生代全能新星的罗云熙和孔垂楠不仅拥有雕刻般帅气的五官，自身青春潮流的气质更是与品牌传达的时尚理念非常契合。一边是国际潮流品牌，一边是极富人气的年轻偶像，本次时装周之行不仅深受品牌方好评，更是获得了主流娱乐媒体的高度关注。新娱儿、唱吧联手跨界，打开了明星×潮流的时尚新格局。</p><p style="box-sizing: border-box; white-space: normal; background-color: rgb(255, 255, 255); color: rgb(51, 51, 51); font-family: 微软雅黑, Helvetica, STHeiTi, sans-seri; text-indent: 2em;">区别于传统媒体出征时装周，新娱儿×唱吧此次时装周的跨界合作不仅让两位潮男亮相秀场，更是促成两位新人玩转FOB showroom，与品牌设计师零距离交流心得。在现场，罗云熙、孔垂楠对各种时尚潮物爱不释手，展现了独特的时尚品味。除此之外，在整个时装周期间，新娱儿×唱吧还特地安排了两位艺人与粉丝进行亲密互动，亲手为心爱的粉丝们挑选精美礼物并于新娱儿官方微信平台进行互动抽奖。<img src="http://mp.sohu.com/editor/themes/default/images/spacer.gif" style="box-sizing: border-box; border-style: none; max-width: 100%;"/></p><p><br/></p>', '2018巴黎时装周新娱儿打造娱乐潮流新格局', null, null, null, '1', null, '1516819077', '1516819077', '1', null);
INSERT INTO "public"."ent_news" VALUES ('9', null, '军事新闻', '军事新闻', '5', '/AbsSR/web/upload/5a6900a0a87f5.png', '<p>军事新闻军事新闻军事新闻军事新闻军事新闻</p>', '军事新闻', '1', null, null, '1', null, '1516830889', '1516830889', '1', null);

-- ----------------------------
-- Table structure for ent_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."ent_user";
CREATE TABLE "public"."ent_user" (
"id" int4 NOT NULL,
"username" varchar(254) COLLATE "default",
"password" varchar(254) COLLATE "default",
"qq" varchar(254) COLLATE "default",
"weixin" varchar(254) COLLATE "default",
"telephone" varchar(254) COLLATE "default",
"email" varchar(254) COLLATE "default",
"last_login_ip" varchar(254) COLLATE "default",
"listorder" int4,
"status" int4,
"create_time" varchar(254) COLLATE "default",
"update_time" varchar(254) COLLATE "default",
"token" varchar(255) COLLATE "C.UTF-8",
"timeout" int4,
"image" varchar(255) COLLATE "default",
"sex" int4,
"last_login_time" int4
)
WITH (OIDS=FALSE)

;
COMMENT ON TABLE "public"."ent_user" IS '客户端登录的用户列表';
COMMENT ON COLUMN "public"."ent_user"."listorder" IS '排序权重';
COMMENT ON COLUMN "public"."ent_user"."status" IS '数据状态';
COMMENT ON COLUMN "public"."ent_user"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."ent_user"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."ent_user"."token" IS '请求验证的token值';
COMMENT ON COLUMN "public"."ent_user"."timeout" IS 'token的失效时间';
COMMENT ON COLUMN "public"."ent_user"."image" IS '头像地址';
COMMENT ON COLUMN "public"."ent_user"."sex" IS '性别';
COMMENT ON COLUMN "public"."ent_user"."last_login_time" IS '最后一次的登录时间';

-- ----------------------------
-- Records of ent_user
-- ----------------------------

-- ----------------------------
-- Table structure for ent_user_news
-- ----------------------------
DROP TABLE IF EXISTS "public"."ent_user_news";
CREATE TABLE "public"."ent_user_news" (
"id" int4 NOT NULL,
"ent_id" int4,
"ent_id2" int4
)
WITH (OIDS=FALSE)

;
COMMENT ON TABLE "public"."ent_user_news" IS '用户新闻关系列表（点赞表）';

-- ----------------------------
-- Records of ent_user_news
-- ----------------------------

-- ----------------------------
-- Table structure for ent_version
-- ----------------------------
DROP TABLE IF EXISTS "public"."ent_version";
CREATE TABLE "public"."ent_version" (
"id" int4 NOT NULL,
"app_type" varchar(255) COLLATE "default" DEFAULT NULL::character varying,
"version" char(255) COLLATE "default" DEFAULT NULL::bpchar,
"version_code" char(255) COLLATE "default" DEFAULT NULL::bpchar,
"is_force" int4,
"apk_url" varchar(4000) COLLATE "default" DEFAULT NULL::character varying,
"upgrade_point" varchar(4000) COLLATE "default" DEFAULT NULL::character varying,
"status" int4,
"create_time" timestamp(6),
"update_time" timestamp(6)
)
WITH (OIDS=FALSE)

;
COMMENT ON TABLE "public"."ent_version" IS 'app版本数据表
';
COMMENT ON COLUMN "public"."ent_version"."app_type" IS 'app的类型值为ios或者android';
COMMENT ON COLUMN "public"."ent_version"."version" IS '内部版本号';
COMMENT ON COLUMN "public"."ent_version"."version_code" IS '外部版本号';
COMMENT ON COLUMN "public"."ent_version"."is_force" IS '是否强制更新';
COMMENT ON COLUMN "public"."ent_version"."apk_url" IS '安装包的地址';
COMMENT ON COLUMN "public"."ent_version"."upgrade_point" IS '更新说明';
COMMENT ON COLUMN "public"."ent_version"."status" IS '状态';
COMMENT ON COLUMN "public"."ent_version"."create_time" IS '创建时间戳';
COMMENT ON COLUMN "public"."ent_version"."update_time" IS '更新时间戳';

-- ----------------------------
-- Records of ent_version
-- ----------------------------
INSERT INTO "public"."ent_version" VALUES ('3', '1', '去问                                                                                                                                                                                                                                                             ', '1                                                                                                                                                                                                                                                              ', '1', '去问', '去问', '1', '2018-01-30 14:12:30', null);
INSERT INTO "public"."ent_version" VALUES ('4', '1', '1                                                                                                                                                                                                                                                              ', '2                                                                                                                                                                                                                                                              ', '1', '11', '111', '1', '2018-01-30 14:17:56', null);
INSERT INTO "public"."ent_version" VALUES ('5', '2', '1.0                                                                                                                                                                                                                                                            ', '1                                                                                                                                                                                                                                                              ', '2', 'http://192.168.0.188/AbsSR/web/app_dev.php/version/list', '更新优化了加载速度和部分界面', '1', '2018-01-30 14:38:40', null);
INSERT INTO "public"."ent_version" VALUES ('6', '1', '1.0                                                                                                                                                                                                                                                            ', '3                                                                                                                                                                                                                                                              ', '2', 'http://www.symfonychina.com/doc/current/templating.html', '新加入注册功能', '1', '2018-01-30 15:27:13', null);
INSERT INTO "public"."ent_version" VALUES ('7', '2', '阿萨德                                                                                                                                                                                                                                                            ', '1.1                                                                                                                                                                                                                                                            ', '1', '阿萨德', '阿诗丹顿', '1', '2018-01-30 15:28:43', null);
INSERT INTO "public"."ent_version" VALUES ('8', '2', '阿萨德                                                                                                                                                                                                                                                            ', '1.2                                                                                                                                                                                                                                                            ', '1', '阿诗丹顿', '阿诗丹顿', '1', '2018-01-30 15:29:03', null);

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."ent_active_id_seq" OWNED BY "ent_active"."id";
ALTER SEQUENCE "public"."ent_admin_user_id_seq" OWNED BY "ent_admin_user"."id";
ALTER SEQUENCE "public"."ent_news_id_seq" OWNED BY "ent_news"."id";

-- ----------------------------
-- Primary Key structure for table ent_active
-- ----------------------------
ALTER TABLE "public"."ent_active" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ent_admin_user
-- ----------------------------
CREATE UNIQUE INDEX "ent_admin_user_pk" ON "public"."ent_admin_user" USING btree ("id");

-- ----------------------------
-- Primary Key structure for table ent_admin_user
-- ----------------------------
ALTER TABLE "public"."ent_admin_user" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ent_app_active
-- ----------------------------
CREATE UNIQUE INDEX "ent_app_active_pk" ON "public"."ent_app_active" USING btree ("id");
CREATE INDEX "relationship_7_fk" ON "public"."ent_app_active" USING btree ("ent_id");

-- ----------------------------
-- Primary Key structure for table ent_app_active
-- ----------------------------
ALTER TABLE "public"."ent_app_active" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table ent_cai
-- ----------------------------
ALTER TABLE "public"."ent_cai" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ent_comment
-- ----------------------------
CREATE UNIQUE INDEX "ent_comment_pk" ON "public"."ent_comment" USING btree ("id");
CREATE INDEX "relationship_2_fk" ON "public"."ent_comment" USING btree ("ent_id");
CREATE INDEX "relationship_4_fk" ON "public"."ent_comment" USING btree ("ent_id2");

-- ----------------------------
-- Primary Key structure for table ent_comment
-- ----------------------------
ALTER TABLE "public"."ent_comment" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ent_crab
-- ----------------------------
CREATE UNIQUE INDEX "ent_crab_pk" ON "public"."ent_crab" USING btree ("id");
CREATE INDEX "relationship_6_fk" ON "public"."ent_crab" USING btree ("ent_id");

-- ----------------------------
-- Primary Key structure for table ent_crab
-- ----------------------------
ALTER TABLE "public"."ent_crab" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ent_news
-- ----------------------------
CREATE UNIQUE INDEX "ent_news_pk" ON "public"."ent_news" USING btree ("id");
CREATE INDEX "relationship_1_fk" ON "public"."ent_news" USING btree ("ent_id");

-- ----------------------------
-- Primary Key structure for table ent_news
-- ----------------------------
ALTER TABLE "public"."ent_news" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ent_user
-- ----------------------------
CREATE INDEX "ent_user_phone" ON "public"."ent_user" USING btree ("telephone");
CREATE UNIQUE INDEX "ent_user_pk" ON "public"."ent_user" USING btree ("id");
CREATE INDEX "ent_user_token" ON "public"."ent_user" USING btree ("token" COLLATE "C.UTF-8");

-- ----------------------------
-- Primary Key structure for table ent_user
-- ----------------------------
ALTER TABLE "public"."ent_user" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ent_user_news
-- ----------------------------
CREATE UNIQUE INDEX "ent_user_news_pk" ON "public"."ent_user_news" USING btree ("id");
CREATE INDEX "relationship_3_fk" ON "public"."ent_user_news" USING btree ("ent_id2");
CREATE INDEX "relationship_5_fk" ON "public"."ent_user_news" USING btree ("ent_id");

-- ----------------------------
-- Primary Key structure for table ent_user_news
-- ----------------------------
ALTER TABLE "public"."ent_user_news" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ent_version
-- ----------------------------
CREATE UNIQUE INDEX "ent_version_pk" ON "public"."ent_version" USING btree ("id");

-- ----------------------------
-- Primary Key structure for table ent_version
-- ----------------------------
ALTER TABLE "public"."ent_version" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "public"."ent_app_active"
-- ----------------------------
ALTER TABLE "public"."ent_app_active" ADD FOREIGN KEY ("ent_id") REFERENCES "public"."ent_version" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."ent_comment"
-- ----------------------------
ALTER TABLE "public"."ent_comment" ADD FOREIGN KEY ("ent_id") REFERENCES "public"."ent_news" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."ent_comment" ADD FOREIGN KEY ("ent_id2") REFERENCES "public"."ent_user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."ent_crab"
-- ----------------------------
ALTER TABLE "public"."ent_crab" ADD FOREIGN KEY ("ent_id") REFERENCES "public"."ent_version" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."ent_news"
-- ----------------------------
ALTER TABLE "public"."ent_news" ADD FOREIGN KEY ("ent_id") REFERENCES "public"."ent_admin_user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."ent_user_news"
-- ----------------------------
ALTER TABLE "public"."ent_user_news" ADD FOREIGN KEY ("ent_id") REFERENCES "public"."ent_user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."ent_user_news" ADD FOREIGN KEY ("ent_id2") REFERENCES "public"."ent_news" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
