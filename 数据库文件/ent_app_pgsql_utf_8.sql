/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     2017/12/27 下午 03:11:14                       */
/*==============================================================*/


-- drop index ent_admin_user_PK;

-- drop table ent_admin_user;

-- drop index Relationship_7_FK;

-- drop index ent_app_active_PK;

-- drop table ent_app_active;

-- drop index Relationship_4_FK;

-- drop index Relationship_2_FK;

-- drop index ent_comment_PK;

-- drop table ent_comment;

-- drop index Relationship_6_FK;

-- drop index ent_crab_PK;

-- drop table ent_crab;

-- drop index Relationship_1_FK;

-- drop index ent_news_PK;

-- drop table ent_news;

-- drop index ent_user_PK;

-- drop table ent_user;

-- drop index Relationship_5_FK;

-- drop index Relationship_3_FK;

-- drop index ent_user_news_PK;

-- drop table ent_user_news;

-- drop index ent_version_PK;

-- drop table ent_version;

/*==============================================================*/
/* Table: ent_admin_user                                        */
/*==============================================================*/
create table ent_admin_user (
   id                   INT4                 not null,
   username             VARCHAR(254)         null,
   password             VARCHAR(254)         null,
   qq                   VARCHAR(254)         null,
   weixin               VARCHAR(254)         null,
   telephone            VARCHAR(254)         null,
   email                VARCHAR(254)         null,
   last_login_date      DATE                 null,
   last_login_ip        VARCHAR(254)         null,
   listorder            INT4                 null,
   status               INT4                 null,
   create_time          VARCHAR(254)         null,
   update_time          VARCHAR(254)         null,
   constraint PK_ENT_ADMIN_USER primary key (id)
);

comment on table ent_admin_user is
'后台用户表';

comment on column ent_admin_user.id is
'主键id';

comment on column ent_admin_user.username is
'用户名';

comment on column ent_admin_user.password is
'密码';

comment on column ent_admin_user.qq is
'qq号码';

comment on column ent_admin_user.weixin is
'微信号';

comment on column ent_admin_user.telephone is
'手机号码';

comment on column ent_admin_user.email is
'邮箱';

comment on column ent_admin_user.last_login_date is
'最后一次的登录时间';

comment on column ent_admin_user.last_login_ip is
'最有一次的登录ip';

comment on column ent_admin_user.listorder is
'排序规则';

comment on column ent_admin_user.status is
'用户状态';

comment on column ent_admin_user.create_time is
'创建时间';

comment on column ent_admin_user.update_time is
'更新时间';

/*==============================================================*/
/* Index: ent_admin_user_PK                                     */
/*==============================================================*/
create unique index ent_admin_user_PK on ent_admin_user (
id
);

/*==============================================================*/
/* Table: ent_app_active                                        */
/*==============================================================*/
create table ent_app_active (
   id                   INT4                 not null,
   ent_id               INT4                 null,
   constraint PK_ENT_APP_ACTIVE primary key (id)
);

comment on table ent_app_active is
'app启动表
';

/*==============================================================*/
/* Index: ent_app_active_PK                                     */
/*==============================================================*/
create unique index ent_app_active_PK on ent_app_active (
id
);

/*==============================================================*/
/* Index: Relationship_7_FK                                     */
/*==============================================================*/
create  index Relationship_7_FK on ent_app_active (
ent_id
);

/*==============================================================*/
/* Table: ent_comment                                           */
/*==============================================================*/
create table ent_comment (
   id                   INT4                 not null,
   ent_id               INT4                 null,
   ent_id2              INT4                 null,
   constraint PK_ENT_COMMENT primary key (id)
);

comment on table ent_comment is
'评论表';

comment on column ent_comment.ent_id2 is
'人员主键';

/*==============================================================*/
/* Index: ent_comment_PK                                        */
/*==============================================================*/
create unique index ent_comment_PK on ent_comment (
id
);

/*==============================================================*/
/* Index: Relationship_2_FK                                     */
/*==============================================================*/
create  index Relationship_2_FK on ent_comment (
ent_id
);

/*==============================================================*/
/* Index: Relationship_4_FK                                     */
/*==============================================================*/
create  index Relationship_4_FK on ent_comment (
ent_id2
);

/*==============================================================*/
/* Table: ent_crab                                              */
/*==============================================================*/
create table ent_crab (
   id                   INT4                 not null,
   ent_id               INT4                 null,
   constraint PK_ENT_CRAB primary key (id)
);

comment on table ent_crab is
'APP异常性能数据表';

/*==============================================================*/
/* Index: ent_crab_PK                                           */
/*==============================================================*/
create unique index ent_crab_PK on ent_crab (
id
);

/*==============================================================*/
/* Index: Relationship_6_FK                                     */
/*==============================================================*/
create  index Relationship_6_FK on ent_crab (
ent_id
);

/*==============================================================*/
/* Table: ent_news                                              */
/*==============================================================*/
create table ent_news (
   id                   INT4                 not null,
   ent_id               INT4                 null,
   title                VARCHAR(254)         null,
   small_title          VARCHAR(254)         null,
   caiid                INT4                 null,
   image                VARCHAR(254)         null,
   content              TEXT                 null,
   description          VARCHAR(254)         null,
   is_position          INT4                 null,
   is_head_figure       INT4                 null,
   is_allowcomments     INT4                 null,
   listorder            INT4                 null,
   source_type          INT4                 null,
   create_time          VARCHAR(254)         null,
   update_time          VARCHAR(254)         null,
   status               INT4                 null,
   constraint PK_ENT_NEWS primary key (id)
);

comment on table ent_news is
'娱乐新闻列表
';

comment on column ent_news.ent_id is
'主键id';

comment on column ent_news.title is
'新闻的标题';

comment on column ent_news.small_title is
'新闻的副标题';

comment on column ent_news.caiid is
'类别id';

comment on column ent_news.image is
'新闻图片的存储路径';

comment on column ent_news.content is
'新闻内容';

comment on column ent_news.description is
'新闻的简单描述';

comment on column ent_news.is_position is
'是否推荐';

comment on column ent_news.is_head_figure is
'是否在首页滚动显示';

comment on column ent_news.is_allowcomments is
'是否允许评论';

comment on column ent_news.listorder is
'排序权重';

comment on column ent_news.source_type is
'新闻来源类型';

comment on column ent_news.create_time is
'创建时间';

comment on column ent_news.update_time is
'更新时间';

comment on column ent_news.status is
'文章状态';

/*==============================================================*/
/* Index: ent_news_PK                                           */
/*==============================================================*/
create unique index ent_news_PK on ent_news (
id
);

/*==============================================================*/
/* Index: Relationship_1_FK                                     */
/*==============================================================*/
create  index Relationship_1_FK on ent_news (
ent_id
);

/*==============================================================*/
/* Table: ent_user                                              */
/*==============================================================*/
create table ent_user (
   id                   INT4                 not null,
   username             VARCHAR(254)         null,
   password             VARCHAR(254)         null,
   qq                   VARCHAR(254)         null,
   weixin               VARCHAR(254)         null,
   telephone            VARCHAR(254)         null,
   email                VARCHAR(254)         null,
   last_login_date      DATE                 null,
   last_login_ip        VARCHAR(254)         null,
   listorder            INT4                 null,
   status               INT4                 null,
   create_time          VARCHAR(254)         null,
   update_time          VARCHAR(254)         null,
   constraint PK_ENT_USER primary key (id)
);

comment on table ent_user is
'用户列表';

comment on column ent_user.id is
'人员主键';

comment on column ent_user.username is
'用户名';

comment on column ent_user.password is
'用户密码';

comment on column ent_user.qq is
'用户qq';

comment on column ent_user.weixin is
'用户微信';

comment on column ent_user.telephone is
'电话';

comment on column ent_user.email is
'用户邮箱';

comment on column ent_user.last_login_date is
'最近一次的登录时间';

comment on column ent_user.last_login_ip is
'最近一次的登录ip';

comment on column ent_user.listorder is
'排序权重';

comment on column ent_user.status is
'用户状态';

comment on column ent_user.create_time is
'用户创建时间（时间戳记录）';

comment on column ent_user.update_time is
'用户信息更新时间（时间戳记录）';

/*==============================================================*/
/* Index: ent_user_PK                                           */
/*==============================================================*/
create unique index ent_user_PK on ent_user (
id
);

/*==============================================================*/
/* Table: ent_user_news                                         */
/*==============================================================*/
create table ent_user_news (
   id                   INT4                 not null,
   ent_id               INT4                 null,
   ent_id2              INT4                 null,
   constraint PK_ENT_USER_NEWS primary key (id)
);

comment on table ent_user_news is
'用户新闻关系列表（点赞表）';

comment on column ent_user_news.ent_id is
'人员主键';

/*==============================================================*/
/* Index: ent_user_news_PK                                      */
/*==============================================================*/
create unique index ent_user_news_PK on ent_user_news (
id
);

/*==============================================================*/
/* Index: Relationship_3_FK                                     */
/*==============================================================*/
create  index Relationship_3_FK on ent_user_news (
ent_id2
);

/*==============================================================*/
/* Index: Relationship_5_FK                                     */
/*==============================================================*/
create  index Relationship_5_FK on ent_user_news (
ent_id
);

/*==============================================================*/
/* Table: ent_version                                           */
/*==============================================================*/
create table ent_version (
   id                   INT4                 not null,
   constraint PK_ENT_VERSION primary key (id)
);

comment on table ent_version is
'app版本数据表
';

/*==============================================================*/
/* Index: ent_version_PK                                        */
/*==============================================================*/
create unique index ent_version_PK on ent_version (
id
);

alter table ent_app_active
   add constraint FK_ENT_APP__RELATIONS_ENT_VERS foreign key (ent_id)
      references ent_version (id)
      on delete restrict on update restrict;

alter table ent_comment
   add constraint FK_ENT_COMM_RELATIONS_ENT_NEWS foreign key (ent_id)
      references ent_news (id)
      on delete restrict on update restrict;

alter table ent_comment
   add constraint FK_ENT_COMM_RELATIONS_ENT_USER foreign key (ent_id2)
      references ent_user (id)
      on delete restrict on update restrict;

alter table ent_crab
   add constraint FK_ENT_CRAB_RELATIONS_ENT_VERS foreign key (ent_id)
      references ent_version (id)
      on delete restrict on update restrict;

alter table ent_news
   add constraint FK_ENT_NEWS_RELATIONS_ENT_ADMI foreign key (ent_id)
      references ent_admin_user (id)
      on delete restrict on update restrict;

alter table ent_user_news
   add constraint FK_ENT_USER_RELATIONS_ENT_NEWS foreign key (ent_id2)
      references ent_news (id)
      on delete restrict on update restrict;

alter table ent_user_news
   add constraint FK_ENT_USER_RELATIONS_ENT_USER foreign key (ent_id)
      references ent_user (id)
      on delete restrict on update restrict;

