/**
 * Created by AbsCD on 2018/1/18.
 */
$(function() {
    $("#file_upload").uploadify({
        swf          : swf,
        uploader     : uploader,
        buttonText   : '图片上传',
        fileObjName  :  'file',
        fileTypeDesc  : 'Image File',
        fileTypeExts  : '*.gif;*.jpg;*.png',
        onUploadSuccess : function(file,data,response){
            //文件上传成功的情况下，显示缩略图，并且把图片文件的路径赋值到隐藏的input控件中
            if(response){
                var _obj = JSON.parse(data);
                $('#upload_org_code_img').css('display','block');
                $('#upload_org_code_img').attr('src',_obj.showpathinfo);
                $('#file_upload_image').val(_obj.showpathinfo);
            }
        }
    });
});